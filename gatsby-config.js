const siteMetadataQuery = `
{
  site {
    siteMetadata {
      title
      description
      siteUrl
    }
  }
}`

const postForRssPostsQuery = `
{
  allMarkdownRemark(
    limit: 1000,
    sort: { order: DESC, fields: [frontmatter___date] },
    filter: {frontmatter: { draft: { ne: true }, layout: { eq: "Post" }}}
  ) {
    edges {
      node {
        excerpt
        html
        frontmatter {
          title
          date
          path
        }
      }
    }
  }
}`

const postForRssLinksQuery = `
{
  allMarkdownRemark(
    limit: 1000,
    sort: { order: DESC, fields: [frontmatter___date] },
    filter: {frontmatter: { draft: { ne: true }, layout: { eq: "Link" }}}
  ) {
    edges {
      node {
        frontmatter {
          title
          link
        }
        html
      }
    }
  }
}`

module.exports = {
	siteMetadata: {
		title: `Coderstand`,
		author: `Brice Coquereau`,
		description: `My blog !`,
		siteUrl: `https://brice.coquereau.fr/`,
		social: {
			twitter: `BriceCq`,
			mastodonInstance: `mamot.fr`,
			mastodonUser: `briced`,
		},
	},
	plugins: [
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/content`,
				name: `blog`,
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/static`,
				name: `assets`,
			},
		},
		{
			resolve: `gatsby-transformer-remark`,
			options: {
				plugins: [
					{
						resolve: `gatsby-remark-images`,
						options: {
							maxWidth: 590,
						},
					},
					{
						resolve: `gatsby-remark-responsive-iframe`,
						options: {
							wrapperStyle: `margin-bottom: 1.0725rem`,
						},
					},
					`gatsby-remark-prismjs`,
					`gatsby-remark-copy-linked-files`,
					`gatsby-remark-smartypants`,
				],
			},
		},
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		{
			resolve: `gatsby-plugin-feed`,
			options: {
				query: siteMetadataQuery,
				feeds: [
					{
						serialize: ({ query: { site, allMarkdownRemark } }) => {
							return allMarkdownRemark.edges.map(edge => {
								return {
									...edge.node.frontmatter,
									description: edge.node.excerpt,
									url: `${site.siteMetadata.siteUrl}${edge.node
										.frontmatter.path}`,
									guid: `${site.siteMetadata.siteUrl}${edge.node
										.frontmatter.path}`,
									custom_elements: [
										{
											'content:encoded': edge.node.html,
										},
									],
								}
							})
						},
						query: postForRssPostsQuery,
						output: '/rss-posts.xml',
						feedTitle: 'Coderstand - les posts',
					},
					{
						serialize: ({ query: { site, allMarkdownRemark } }) => {
							return allMarkdownRemark.edges.map(edge => {
								return {
									...edge.node.frontmatter,
									description: edge.node.html,
									url: `${edge.node.frontmatter.link}`,
									guid: `${edge.node.frontmatter.link}`,
									custom_elements: [
										{
											'content:encoded': edge.node.html,
										},
									],
								}
							})
						},
						query: postForRssLinksQuery,
						output: '/rss-links.xml',
						feedTitle: 'Coderstand - les liens',
					},
				],
			},
		},
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `Gatsby Starter Blog`,
				short_name: `GatsbyJS`,
				start_url: `/`,
				background_color: `#ffffff`,
				theme_color: `#663399`,
				display: `minimal-ui`,
				icon: `content/images/gatsby-icon.png`,
			},
		},
		`gatsby-plugin-offline`,
		`gatsby-plugin-react-helmet`,
		{
			resolve: `gatsby-plugin-typography`,
			options: {
				pathToConfigModule: `src/utils/typography`,
			},
		},
	],
}
