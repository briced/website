const moment = require('moment')

module.exports = function(plop) {
    const dateForUrl = moment().format('YYYY/MM/DD')
    const dateNextWeekForUrl = moment()
        .add(7, 'days')
        .format('YYYY/MM/DD')
    plop.setPartial('date', `${moment().format('YYYY-MM-DDTHH:mm:ssZ')}`)
    plop.setPartial(
        'dateNextWeek',
        `${moment()
            .add(7, 'days')
            .format('YYYY-MM-DDTHH:mm:ssZ')}`
    )
    plop.setPartial('dateForUrl', `${dateForUrl}`)
    plop.setPartial('dateNextWeekForUrl', `${dateNextWeekForUrl}`)
    plop.setActionType('log', function(answers, config, plop) {
        return plop.renderString(config.str, answers)
    })

    plop.setGenerator('link', {
        description: 'create a link',
        prompts: [
            {
                type: 'input',
                name: 'name',
                message: 'name',
            },
            {
                type: 'input',
                name: 'url',
                message: 'url',
            },
            {
                type: 'input',
                name: 'tag',
                message: 'tag',
            },
            {
                type: 'input',
                name: 'content',
                message: 'content',
            },
        ],
        actions: [
            {
                type: 'add',
                path: `content/links/${dateForUrl}/{{name}}.md`,
                templateFile: 'plop-templates/link.md',
            },
            {
                type: 'log',
                str: `content/links/${dateForUrl}/{{name}}.md`,
            },
        ],
    })

    plop.setGenerator('post', {
        description: 'create a post',
        prompts: [
            {
                type: 'input',
                name: 'title',
                message: 'title',
            },
        ],
        actions: [
            {
                type: 'add',
                path: `content/posts/${dateForUrl} - {{dashCase title}}.md`,
                templateFile: 'plop-templates/post.md',
            },
            {
                type: 'log',
                str: `content/posts/${dateForUrl} - {{dashCase title}}.md`,
            },
        ],
    })

    plop.setGenerator('lienstropbien', {
        description: 'create a "les liens trop bien" post for next week',
        prompts: [
            {
                type: 'input',
                name: 'number',
                message: 'number',
            },
        ],
        actions: [
            {
                type: 'add',
                path: `content/posts/${dateNextWeekForUrl} - les-liens-trop-bien-{{number}}.md`,
                templateFile: 'plop-templates/liens_trop_bien_next_week.md',
            },
            {
                type: 'log',
                str: `content/posts/${dateNextWeekForUrl} - les-liens-trop-bien-{{number}}.md`,
            },
        ],
    })

    plop.setGenerator('meetup', {
        description: 'create a meetup report',
        prompts: [
            {
                type: 'input',
                name: 'title',
                message: 'title',
            },
        ],
        actions: [
            {
                type: 'add',
                path: `content/meetups/${dateForUrl} - {{dashCase title}}.md`,
                templateFile: 'plop-templates/meetup.md',
            },
            {
                type: 'log',
                str: `content/meetups/${dateForUrl} - {{dashCase title}}.md`,
            },
        ],
    })
}
