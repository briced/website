---
uuid: "5cbf4af8-82f0-11e5-801b-0fe2952175fb"
title: "Be motivated"
isLink: true
layout: "motivator"
links:
- http://www.positivityblog.com/index.php/2014/12/04/2-minutes-a-day/
- http://tinybuddha.com/blog/8-great-habits-that-hold-you-back-from-actual-happiness/
- http://tinybuddha.com/blog/stop-fearing-worst-worrying-about-what-ifs/
- http://tinybuddha.com/blog/7-ways-live-less-fearful-peaceful-life/
- http://tinybuddha.com/blog/simple-5-minute-habit-can-make-happier/
- http://tinybuddha.com/blog/transforming-self-criticism-stop-trying-to-fix-yourself/
- http://tinybuddha.com/blog/19-simple-daily-habits-for-a-happier-life/
- http://tinybuddha.com/blog/6-fears-that-keep-you-busy-instead-of-enjoying-life/
- http://tinybuddha.com/blog/5-things-stopping-you-from-achieving-your-goals/
- http://www.inwealthandhealth.com/inherit-7-habits-of-insanely-successful-people/
- http://tinybuddha.com/blog/3-steps-instantly-boost-self-esteem/
- http://zenhabits.net/creation/
sentences:
- "You are a good person"
- "Relax"
- "Trust yourself"
- "Love yourself"
- "Breathe slowly"
- "This day is unique. Enjoy it."
- "Change your thoughts and <br/>you change your world."
- "You are not perfect. Nobody is. <br/>And that's okay"
- "Hug harder.<br/>Laugh louder.<br/>Smile bigger.<br/>Love longer."
- "Compare yourself to yourself."
- "Know yourself"
- "Enjoy your life"
- "Go slow if you need to"
- "You are here"
- "Stop hating yourself <br/>for everything you aren’t.<br/>Start loving yourself<br/>for everything that you are."
- "Forgive your flaws"
- "Enjoy it all"
- "Meditate"
- "Stop overthinking"
- "You’ll suck at it. That’s ok"
- "Do or do not. There is no try."
---
