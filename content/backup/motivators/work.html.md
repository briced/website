---
uuid: "9ae7430c-82e7-11e5-a3cb-0bf03a6b9cf8"
title: "Be motivated"
isLink: true
layout: "motivator"
sentences:
- "Do what you love<br/>Love what you do"
- "Fake it until you make it"
- "Don’t confuse the urgent with the important"
---
