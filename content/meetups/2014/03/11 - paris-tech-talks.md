---
title: "Paris Tech talks (11/03/2014)"
date: "2014-03-11T12:56:36+01:00"
layout: Meetup
path: "/2014/03/11/fr/paris-tech-talks/"
---
# 1er talk: Machine learning @ Criteo

Achat d'espaces publicitaires.

Real time bidding (RTB).

Machine: click prediction model.

Contraintes de temps - réponses en ms.

Données de prises de décisions, beaucoup de possibilités.

C# / Hadoop.

Item prediction: 1st version: last item user saw. Efficace. Maintenant: ne pas se fier au + vendu. Toujours prendre en compte le user.

__Instant pub__: Paymill. Affiliate program. Début en avril.

# 2eme talk: Hadoop quick start on Google Cloud.
Le tuto du site, mais en live.

Qubole. Hadoop as a service. Se connecte sur Google Cloud et s'occupe de gérer Hadoop.

__Instant pub__ 99 designs. Design for small businesses.

__Instant pub__ Rails girls.

# 3eme talk: Mobile image recognition - Moodstocks
Codé en C99.
Les utilisateurs d'API choisissent quelles images sont synchronisées sur le device.

__Instant pub__ dotScale.

# Lightning talk 1: Paris Data Geeks. 4 avril.

# Lightning talk 2: Performance User Group.
