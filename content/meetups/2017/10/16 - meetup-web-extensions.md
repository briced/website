---
title: "Meetup WebExtensions (16/10/2017)"
date: "2017-10-16T12:51:44+01:00"
layout: Meetup
path: "/2017/10/16/fr/meetup-web-extensions/"
---
# Prez globale
Firefox 57 Quantum

Avant 32000 extensions

Choix position :
- barre de boutons
- barre d'url

Potentiel:
- Sidebar
- clic droit
- Suggestions de recherche
- notifications
- configuration
- dev tools

Aujourd'hui 5180 extensions prêtes pour 57

manifest.json
- Browser action: bouton
- Page action: barre d'url (html) 
- Option page: options_ui (html)
- Sidebar: sidebar_action (html)

# Use case: iGraal
API: browser.tabs

> webextension-polyfill
