---
title: "Meetup Java 9 (31/10/2017)"
date: "2017-10-31T12:49:37+01:00"
layout: Meetup
path: "/2017/10/31/fr/meetup-java-9/"
---
# Nouveautés
- Système de module JPMS (Jigsaw)
- JDK modularisé
- G1 nouveau GC
- changement de numérotation de versions
- Jshell
- StackWalking API
- AOT compiler (experimental)

- méthodes privées dans les interfaces
- Reactive Stream (4 interfaces)
- Api process: gestion des processus depuis Java
- Fabriques pour les collections immutables (`Set.of`, `List.of`, `Map.of`)
- Compact Strings
- Annotations `@Deprecated(forRemoval)`

# Classpath Hell
- résolution par ordre alphabétique
- Luck based

JVM pré-9 laxiste `sun.misc.Unsafe` ou `field.setAccessible(true)`

# JVM Java 9
- plus de rt.jar et tools.jar
- JMods format pour la JVM
- Images personnalisées
- JDK modulaire

## un module
- exporte des packages
- confine des packages (internals)
- description dans module-info.java
- ⚠️ public change de sens si la classe n'est pas exportée.

Avantages:
- check à compile et runtime
- les classes d'un package sont chargées depuis un seul jar
- chargement plus rapide
- Sanity check: un package ne peut exister dans 2 modules différents. ⚠️ également sur les packages non exposés
- strong encapsulation

`requires` VS `requires static` VS `requires transitive`

`exports` VS `exports to` VS `opens`

`provides with` et `uses`

Outil: jdeps

## Compilation
`javac --module-path /modules`

Linking: `jlink`
