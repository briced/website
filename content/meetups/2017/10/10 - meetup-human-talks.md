---
title: "Meetup Human Talks (10/10/2017)"
date: "2017-10-10T12:54:06+01:00"
layout: Meetup
path: "/2017/10/10/fr/meetup-human-talks/"
---
# Talk #1: Mythes UX
## 1) les gens lisent
- 50% d'un article n'est jamais affiché
- 20% est lu

## 2) les gens ne scrollent pas
- 90% des mobiles vont jusqu'en bas de la page

## 3) tout le site doit être accessible en 3 clics
Le nombre de clics n'affecte pas la satisfaction et le taux de réussite

## 4) les gens regardent les pubs
Banner blindness

## 5) Copier Google, Amazon, Facebook, Apple
Les géants font de l'AB test pour savoir ce qui marche **pour eux**

## 6) les mobinautes sont distraits
60% de l'usage mobile se fait à la maison
77% des recherches mobiles se font à la maison ou au bureau.

## 7) Pas plus de 7 éléments
Georges Miller 1958. Mémoire court terme. Pas applicable quand les éléments restent affichés.

# Talk #2: HTTPS à quoi ça sert, comment ça marche

Explication cryptographie asymétrique. Clé publique/privée.

Letsencrypt. 

# Talk #3: OptaPlanner
https://www.optaplanner.org/

Résolution de problèmes NP-complet

Trouve une solution proche de l'optimum.

Implémente des algos de construction heuristique, avec recherche locale.

Langage DROOLS http://drools.org/

# Talk #4: pour que le CSS ne soit plus une corvée

- Trop de !important
- Trop de duplication

Astuces:
- maîtriser les outils CSS (flexbox, CSS grid) 
- cibler le Done (IE8?)
- KISS
- découpler les sélecteurs
- garder des sélecteurs de basse priorité
- pas de valeur magique
