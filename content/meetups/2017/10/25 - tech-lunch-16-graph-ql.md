---
title: "TechLunch #16: GraphQL (25/10/2017)"
date: "2017-10-25T21:58:39+01:00"
layout: Meetup
language: en
path: "/2017/10/25/fr/tech-lunch-16-graph-ql/"
---
# Talk #1: Présentation

GraphQL version-free: only add fields, never remove non-nullable fields.

Cache : prefer application cache than network cache.

Security : indépendant from GraphQL.

About DoS: do a cost analysis before executing the query. Cap lists to a maximum. We can also create a query whitelist (persisted queries)

Monitoring : Apollo Engine

Documentation : generated from schema

## Easy to PoC, hard to prod.

# Talk #2 GraphQL real-time with subscriptions

SubscriptionServer/SubscriptionClient

Real-time over GraphQL. Use PubSub.

# Talk #3: GraphQL JS and dataloader

3 layers
- presentation: web server port, login, user security (logged), dispatch GraphQL queries to business classes 
- business: create returned objects, check business rules (does the user has access? What fields can he view?) 
- data access: DB access, FS access, etc

http://knexjs.org/

Problem: each query by ID is separated from the others. Called N+1 problem. 

Solution: add a dataloader layer between data access and business

https://github.com/facebook/dataloader

One dataloader per user.