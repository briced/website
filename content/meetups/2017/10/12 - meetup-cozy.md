---
title: "Meetup Cozy (12/10/2017)"
date: "2017-10-12T12:52:51+01:00"
layout: Meetup
path: "/2017/10/12/fr/meetup-cozy/"
---
# Snips.ai

Déjà vue au meetup AI

# Big data de pair à pair

Mettre en réseau des Cozy pour avoir un système de recommandation au niveau de Netflix, mais avec une anonymisation pour chaque utilisateur.

# État des lieux Cozy

- Store pas fini
- Appli Banque pas finie
- Partage inter-Cozy
- Partage avec des gens qui n'ont pas Cozy

# Connecteurs

