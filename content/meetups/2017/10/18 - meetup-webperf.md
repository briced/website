---
title: "Meetup Webperf (18/10/2017)"
date: "2017-10-18T22:00:07+01:00"
layout: Meetup
path: "/2017/10/18/fr/meetup-webperf/"
---
# Mesurer la webperf
`window.onload`

Le trigger de onload n'est pas représentatif de l'expérience utilisateur.

De meilleures métriques existent:
- temps de premier rendu
Api non officielle

`performance.timing.msFirstPaint`

`performance.getEntriesByType('paint')`

Problème:est-ce un rendu satisfaisant ? Si on affiche un carré vert, non. Il faut du contenu.
- SpeedIndex. Plus il est haut, moins c'est bon. Proche de la vision utilisateur. Mais peut se mélanger les pinceaux sur des pubs ou contenus dynamiques.

- First meaningful paint. Le moment où le navigateur affiche le plus grand nombre de 'layouts' (au sens "layout paint composite") visibles, après les fonts.

- Time to interactive. Attend le first meaningful paint, puis que le CPU n'ait pas de tache de + de 50ms pendant 5s.

Toutes ces mesures sont génériques. On peut trouver ses métriques pertinentes. Exemple : YouTube mesure le time to launched video

Api navigateur : 

`performance.mark()`
`performance.getEntries()`
`PerformanceObserver() `

# Des anims optimisées

`will-change` pour déclarer quelle propriété va être animée.

`requestAnimationFrame` en JS. Préférer ça même pour les anims onScroll. Stocker la valeur dans onScroll, modifier le DOM  dans requestAnimationFrame. Problème : dépend du thread JS. Solution: Web animations Api, qui délègue l'animation au thread de rendering.

Flip technique. On retient la place initiale des éléments, on modifie le DOM pour les mettre en état final, puis en CSS on les remet en position initiale, et on fait une animation de X->0 puisque l'élément dans le DOM est déjà dans son état final.

Next: `contain: layout` `contain: paint`
