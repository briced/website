---
title: "Meetup Identité décentralisée (29/06/2017)"
date: "2017-06-29T13:00:09+01:00"
layout: Meetup
path: "/2017/06/29/fr/meetup-identité-décentralisée/"
---
# "Je signe donc je suis" Enjeux et nature de l'identité numérique

## Présentation

Le numérique a modifié notre rapport à l'identité. 

Avant, c'était l'état civil qui servait de seed. Le passeport était un token (qui pouvait porter d'autres tokens, comme les visas). 

Avec le numérique, il y a bien plus de flux, donc plus de traces numériques. D'où plus de données, qui ont de la valeur. Il y a moins de contrôle sur ces identités, Facebook peut très bien décider de partager notre identité sans notre consentement. 

Il y a eu plus étapes dans l'identité numérique:
- dans chaque service 
- fédéré: oauth
- user-centric: open id, je choisis quelles données je partage à qui
- self sovereign identity: les tiers ne gèrent pas notre identité, mais ils peuvent valider l'information que je veux partager au service A

10 principes (cf blog Chris Allen, the path to self sovereign identity)

Solutions:
- Blockstack
- Uport
- Sovrin
- Aevatar

# En pratique
En 2010, système anonyme de validation de présence au cours. Arrêté en 2014. Validate par la CNIL.

Microsoft: U-Prove

IBM: Identity mixer

### Présentation du token
Présentation policy
Token en réponse, qui valide la policy. Non traceable.
Pseudonyme: un hash

## DIDs (Decentralized identifiers)
Basé sur la blockchain pour stocker les données. Permet la self sovereign identity

Un did (URN, RFC 2141)
did:sov:636282urhbdhxjuehheyfy72uhd

Le préfixe (ici 'sov') sait comment traiter l'id.

DDO descriptor object
Contient:
- did
- public keys
- controlling dids
- timestamps
- endpoints

On peut chaîner les DIDs. 

Plusieurs implementations: JSON web tokens, autre... 

Un guardian gère un DID d'autres personnes. 

5 rules for respecting privacy
-plan for multiple DIDs to enable persona and pseudonym management 
- avoid correlation of keys across DDOs
- avoid correlation of off-lewger pointers across DDOs
- avoid storing data on a public ledger
- use anonymous credentials (zero knowledge proofs) whenever possible