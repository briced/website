---
title: "TechLunch #13 Security (28/06/2017)"
date: "2017-06-28T13:12:34+01:00"
layout: Meetup
language: en
path: "/2017/06/28/fr/tech-lunch-13-security/"
---
# Talk #1: SSH hidden features and how to improve security

## What people do

- Username/password
Better: SSH key pairs

- does not check the host certificate

- using a shared account. Problem: Who did what? 

## Goal
- ephemeral SSH keys
- temporary certificates (10 min) 
- centralize user management 
- really be afraid by host certificate alerts

## What we do not want
- ask user to regenerate their key every day
- make login more difficult
- reimplement crypto
- use user management to configure fine grained access to the server

## How

- SSH has support for certificate
- can enforce authorized commands
- can force IPs
- SSH logs certificate ID
- since openssh 5.7

Go support certificate creation and signing

## New process
- request a certificate 
- tell SSH to use it
- use your own account
- work
- disconnect 

Démo

# Talk #2: How to hack your neighbor's webcam

Vu aux Human Talks: https://youtu.be/dY2296wBJ-Q

Man-In-The-Middle Attack
ARP poisoning

Software:
- nmap (network scan and port scan)
- ettercap
- Wireshark
- Metasploit

# Talk #3: Good security is good UX

Bad security is bad UX. A hack make users leave your site, like bad UX

Good security is bad UX. And bad security. Example: password with 10 rules.

Some good UX is bad security. And bad UX. Example: resenting the password in a mail. Confirming email change by sending email to the previous address. 

Security theater is bad UX.

Conclusion : security and UX go hand-in-hand