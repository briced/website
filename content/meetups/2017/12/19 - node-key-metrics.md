---
title: "Meetup Node @ KeyMetrics (19/12/2017)"
date: "2017-12-19T21:47:41+01:00"
layout: Meetup
path: "/2017/12/19/fr/meetup-node-key-metrics/"
---
# Pub PM2
KeyMetrics va se renommer PM2 premium

-> Regarder le calcul du dependencies score

# IPFS: Decentralize the web content

Aujourd'hui le web est semi centralisé : des serveurs variés hébergent des données.

Avantage : données centralisées

Inconvénients : facile à couper/censurer

## Nouveau modèle : le Web distribué

Chaque noeud est client (s'il n'a pas l'info) ou serveur (s'il a l'info)

Comment identifier la ressource ? CID (Content identifier)

Gain: historisation possible

Défauts: wut url ?

-> Regarder IPNS

# Async Wars

Node perd le contexte très facilement: setTimeout, async, callbacks. Donc les stacktraces sont inutiles.

Solution 1: `require('domain')`. Deprecated. 

Solution 2: userland patching. Closures autour de toutes les méthodes asynchrones. Does not work with async/await.

Solution 3: `async_hooks`

Future: Domains are now implemented as async_hooks.

Future: Zones?