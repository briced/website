---
title: "Meetup Angular #35 (11/12/2017)"
date: "2017-12-11T21:53:02+01:00"
layout: Meetup
path: "/2017/12/11/fr/meetup-angular-35/"
---
# La détection de changements

Trigger par:
- les événements du DOM
- les XHR
- les Timer JS
Bref, tout ce qui est asynchrone.

Pour ça, Angular utilise les Zones. Monkey patch de l'event queue.

Utilisation de OnPush pour faire de la change detection en comparant uniquement les références.

Demo

On peut injecter ChangeDetectorRef. Méthodes reattach() et detach().

# Injection de dépendances dans Angular

Injection par réf ou par token.