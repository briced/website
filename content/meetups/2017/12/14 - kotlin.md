---
title: "Meetup Kotlin (14/12/2017)"
date: "2017-12-14T21:47:41+01:00"
layout: Meetup
path: "/2017/12/14/fr/meetup-kotlin/"
---
# Vert.x

Programmation événementielle non bloquante.

Opti: 2 event loop par core

Coroutines Kotlin. Dans 1.2

Lifecycle: begin suspend resume end

Vert.x: awaitResult, channel support, integrated in kotlin ecosystem

Keyword `suspend`

# KotlinConf
## Common module
- Common modules : compiler en Java et JS
Std lib common.
Platform module = specifique
Standard module = la base

Keyword `expect` et `actual`

Package `kotlinx` multiplateforme. 

Grade `expectedBy`

Keyword `tailrec`
## Natif