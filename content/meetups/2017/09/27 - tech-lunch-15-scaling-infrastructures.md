---
title: "TechLunch #15: Scaling infrastructures (27/09/2017)"
date: "2017-09-27T12:55:02+01:00"
layout: Meetup
language: en
path: "/2017/09/27/fr/tech-lunch-15-scaling-infrastructures/"
---
# Talk 1: the cost of Scaling
## Analyze expenses
Aws explorer

Google mail alerts

## Clean
Clean unused VMs, IPs, snapshots, files.

AWS: check the regions

### Data transfert
Inter-regions transfert = $$$

### Compute
- Use new generations: cheaper and more powerfull
- Reserve instances
- ⚠️ Spot instances

### Serverless is not for everybody

### Multi cloud
Use Terraform/Ansible/Docker

### Go bare metal (OVH)

# Talk 2: Scaling behind the scene (LeaseWeb) 

Datacenter management with APIs

# Talk 3: Scaling apis from 0 to 40k RPM

Monitor: new relic

Provision unused machines, ready when needed. 
