---
title: "Meetup Human Talks (11/07/2017)"
date: "2017-07-11T13:04:04+01:00"
layout: Meetup
path: "/2017/07/11/fr/meetup-human-talks/"
---
# Devenir freelance

Déléguer la gestion comptable, la recherche de clients

## Pourquoi

### l'argent
Vendu plus cher

### liberté
Bosser tout le temps ou bosser 6 mois par an

### apprendre
Plein de missions variées

## Comment
### Démissionner
Vive LinkedIn

### Trouver des clients
Twitter ou SSII

### Gérer sa boite
Déléguer à des comptables

http://www.gahfy.net/blog/2017/07/guide-pratique-developpeur-freelance/

# Jupyter et D3

Contexte: satisfaction client en baisse au fil des sprints. Trop de bugs.

Aggregation entre trello (tâches, flaggés avec le retour client) et git (nom de commits et fichiers modifiés)

Conclusion: utiliser la donnée pour mesurer les taux d'erreurs

# Monté Carlo tree search

UCT

RAVE rapid action value estimation

Progressive unpruning

Optimiser: apprendre des patterns (manuels ou générés) 