---
title: "Meetup Kotlin (18/07/2017)"
date: "2017-07-18T13:08:18+01:00"
layout: Meetup
path: "/2017/07/18/fr/meetup-kotlin/"
---
# Just in time with Kotlin

Use case: Mr Beer with rosé

https://start.spring.io with Kotlin

Kotlin est immutable et Spring utilise l'extension par défaut => utiliser spring compiler plugin

Tests: utiliser lateinit pour les services instanciés par Spring.

Keyword: data pour les classes serialisées par Spring

Penser au null check

`filter(it in 1..3)`

# Présentation association Paris Kotlin User Group

# Why Spring ❤️ Kotlin

`kotlin-spring`
`kotlin-noarg`

## Upgrade to Spring 5

Utiliser les nullable avec Get mapping pour déterminer les params required ou non.

Type reifiés qui utilisent les noms de fonctions, mais sans besoin de passer la classe. `findAll(User.class)`=>`List<User>=findAll()`

## Migrate to WebFlux

Reactive Streams via RxJava / Reactor / Akka Streams

## Using WebFlux functional API

https://github.com/mixitconf/mixit

## Enlever Spring Boot?

## JS? 

Kotlin 1.1.4 avec Dead Code Elimination Plugin

## Future

- Kotlin to WebAssembly
- Spring Framework 5 GA September 2017
- Spring Boot 2 GA in November 2017

https://kotlin.link/

Slides: https://goo.gl/qMA9Ho