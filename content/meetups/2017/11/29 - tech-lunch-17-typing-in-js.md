---
title: "TechLunch #17 Typing in JS (29/11/2017)"
date: "2017-11-29T21:54:21+01:00"
layout: Meetup
language: en
path: "/2017/11/29/fr/tech-lunch-17-typing-in-js/"
---
# Talk #1 Typescript with React
+ Check PropTypes à la compilation
+ PropTypes avec generic types ⚠️ interpolation


# Talk #2 Flow
- Flow écrit en Ocaml 🇫🇷
- Atom / VS Code (WebStorm?) 
- Type inference
- custom types
- union types
- generic types
- seal types {|type|} 
- merge objet types with spread
- objects as map
- read and write properties
- some helpers $keys $Element Type etc etc
- CLI

# Talk #3 Reason
- Ocaml with JS syntax
- auto currying
- module system, open or not
- immutable by default
- variant types = union types
- pattern matching
- local scope
- BuckleScript
- import JS or convert from JS
- https://reasonml.github.io/