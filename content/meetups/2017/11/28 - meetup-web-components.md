---
title: "Meetup WebComponents (28/11/2017)"
date: "2017-11-28T21:56:28+01:00"
layout: Meetup
path: "/2017/11/28/fr/meetup-web-components/"
---
Polymer 3.0 compatible avec Polymer 2

# Slim.js
- Pas de shadow DOM
- aide pour les listeners d'éléments
- obligé d'indiquer clairement les data binding et les ids (`slim-id`)

# Bram.js
- comme Slim.js mais avec Shadow DOM
- model interne à chaque component. Lié automatiquement aux attributs. 

# Skatejs
- utilise un virtual DOM
- compatible React

# Stencil
- next generation Ionic
- écrit du Angular, compile en webcomponent standard 
- virtual DOM, async rendering, Reactive data binding, Typescript, JSX
- SSR ! 

# Regardez Glimmer
