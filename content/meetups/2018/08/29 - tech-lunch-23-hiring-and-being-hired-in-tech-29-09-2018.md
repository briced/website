---
title: "TechLunch #23: Hiring and being hired in tech (29/08/2018)"
date: "2018-08-29T14:27:41+02:00"
layout: Meetup
path: "/2018/08/29/fr/tech-lunch-23-hiring-and-being-hired-in-tech-29-08-2018/"
---
## Being hired

Candidates choose their preferences, but sometimes they don't think of all the points. It's recruiters job to identify the points that make the entreprise shine

A job is more than work. Find about culture, OSS contribution ask old employees, what the product does. During interviews, ask recruiters what they think about the company. Did things they were told during their recrutements were true? Ask also what you can bring on the table.

Where the entreprise is (just starting, seeking funds, growing fast) is also a big criteria.

Identify red flags. In job post, "ninja" and this kind of things. Find out if it's gonna work with your potential manager. Seek honesty. A manager insisting on a framework instead of programming concepts is generally a bad manager.

Soft skills are really important. This is more important than hard skills, especially for small teams.

Know your resume, be ready to talk about every part of it.

Interview are stressful. Do what you can to minimize stress. Ask the candidates about what they did in the past, to reassure them.

## Hiring

Hard skills are easy to write in the offer, soft skills are harder. Autonomy, empathy, attention to details, figure what you really need for your team. Translate soft skills to real world situation. For inclusion, write the minimum needed, because minorities will not apply if they don't match 99% of the requirements. Separate must have skills and nice to have skills.

Know what you want to recruit. A bad offer will bring candidates that won't match. 

Pitch your entreprise. Find out why your job is better than the others. Small team? You are gonna work on all the stack, with visibles results. Big team? World projet, with big challenges.

Have a presence. Blog, meetup, events. You promote diversity? Talk about it. Having a great culture? Talk about it.

Diversity cannot be enforced. You have to articulate the culture to be diverse, but not recruit someone "because he/she is X". Never forget that diversify can challenge your entreprise culture, and it's a good thing, but too much challenge can destroy your initial culture, and other will leave. Hard thing to balance.

## Questions

### How to decide between a recrutement or a freelance? 
Do you need long term investment? Do you need a culture fit? Decide if you need a short term expert, or a long term learner.

### How to identify a lying candidate? 
Candidate: be honest. Truth will be known. Add more to the table than your past mistakes. 

Recruiters: talk with old colleagues, candidate's references. If a candidate is lying, stop right now. 
