---
title: "TechLunch # 18 - Scaling your tech team (31/01/2018)"
date: "2018-01-31T12:00:00+01:00"
layout: Meetup
language: en
path: "/2018/01/31/fr/meetup-techlunch18-scaling-your-tech-team/"
---
## Talk #1 From one to sixty (Algolia)

- Give ownership and freedom to people
- Implement process when problems arises

New concept : Efficiency = Productivity + vision + focus

Challenges : flat organization: slow to deliver, because a person can be on multiple projects. Hard feedback, because no chief. Problems for onboarding. 

Solution: Have squads. With tech leads. Objectives must fit the company vision. 
Solution: Product managers.

Challenge:
Career framework: after 3 years, employees want a vision. 

Solution: fix clear objectives for work evolution. 

Challenge: onboarding

Solution: clear onboarding plan. 3 weeks short term objectives + quarterly objectives

## Talk #2 Engineering at Stripe (Stripe)

2015: 80 engineers / no engineering manager / no product manager

- Trust is local separation. Absolute trust between devs. 
- Open communication is long range attraction. A mailing list for each team cced on each mail. 
- a clearly stated mission and a simple product is alignment

??? Flocking behavior

⚠️ long range attraction is what broke first

Organisation: hiring / ladders / managers / planning / re-orgs

The bigger the team is, the less value a new engineer adds to the team. 

The longer the company, the more employees you need to add to have the same growth. 

## Talk #3 Gazr (Dailymotion)

https://gazr.io/

Micro services in different languages.

To hide complexity, gazr.io is just a specification for creating a Makefile for each projects, with the same commands for every projects.

All is done through Docker.
