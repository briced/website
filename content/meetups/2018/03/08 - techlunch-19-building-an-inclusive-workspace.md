---
title: "Techlunch #19: Building an inclusive workspace (08/03/2018)"
date: "2018-03-08T14:25:35+01:00"
layout: Meetup
path: "/2018/03/08/fr/techlunch-19-building-an-inclusive-workspace-08-03-18/"
---
## Q&A with a 3 women panel
- Erika Batista (director @ The family) 
- Sophie Despeisse (developer @ Toucan Toco) 
- Marion Aguirre (recruter @ Algolia)

Diversity reflects our clients. It allows us to not overlook issues in our projects (like IPhone X and Asians)

Founders solves their problems. White founders solves white problems. Men solves men problems.

Culture should focus on being inclusive, with internal and external communication. Retraining people allows new culture to enter, destroy ideas about schools (this school = cool, this school = bad)

Men applies when they have less skills checked for the same job. Take this biais into consideration. 

Have a culture that encourages attraction and retain for women. Ask womens about that they need, don't imagine it.

There is less things asked by female founders.

There is no fund raise problems by female founders. 

Impostor syndrome is a problem. Women face it more than mens. But sometimes it motivates them to over deliver.

Women asks less for salary as men. It's the company responsability to be responsible the make things equals. 

(personal thinking: maybe negociation brings back to the stereotypes about greedy women, and that's a mental blocker) 

Job posts redaction should be inclusive. A ninja developer riding a rocket is not something a woman may be dreaming about. (personal thinking: problem with having all the needed skills for applying. Nobody is a real ninja)

https://www.hiremorewomenintech.com/

Competition for women is viewed differently than competition between men. 

Jealousy between women is often encouraged by the permanent comparison between women in the workplace. Add impostor syndrome, and women starts to be jealous of each other and fight to "not be the impostor"
