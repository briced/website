---
title: "Human Talks (10/04/2018)"
date: "2018-04-10T23:49:49+02:00"
layout: Meetup
path: "/2018/04/10/fr/human-talks-10-04-2018/"
---
## Talk #1: Passer de SaaS à du On-premise
VMs ? Trop de fournisseurs possibles

Docker ? Trop moderne

Les besoins:
- documentation (+ moteur de recherche) 
- répertoires customs
- feature flag (j'ai déjà Mongo/nginx)
- monitoring (page de status, pourvoir s'interfacer avec des monitoring existants) 
- penser On-premise pour les devs
- avoir un support (mail à minima) 

Être son propre client, du coup le SaaS utilise le On-premise.

## Talk #2: Se lancer dans le podcast

- Se demander pourquoi le faire ? Savoir à partager, modèle qu'on veut imiter, etc etc
- méthodologie SMART
- hébergement. Site statique ou SoundCloud.
- enregistrer. Micro du pc. Casque pour le retour. BlueYeti. GarageBand ou Audacity. 
- diffuser. Réseaux sociaux, RSS

## Talk #3: Faire un moteur de recherche par images en moins de 10 minutes

Avant: annotation manuelle des images. Long et imparfait.

Maintenant: analyse des pixels des images. 

Algos possibles:
- histogrammes des couleurs 
- algorithme de couleur dominante 
- utilisation de banques de filtres pour mettre en relief des aspects de l'image. SIFT
- réseau de neurones convolutionnels pour classification. VGG16. 

Utilisation de l'avant dernière couche comme descripteurs.

## Talk #4: Kubernetes on Raspberry Pi
Kubernetes: orchestrateur de dockers pour du déploiement sur une ferme de serveurs. Pods manipulés par des déploiements.

Ce qui est bien:
- déplacement automatique des applications quand un raspberry s'éteint.
- possibilité de monter des volumes distants
- robuste aux interruptions
- déploiements reproductibles

Moins bien:
- devoir packager son appli sur ARM
- impossibilité de monter des sous-dossiers d'un PersistantVolume
- un peu overkill