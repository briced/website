---
title: "Human Talks (13/02/2018)"
date: "2018-02-13T23:34:12+01:00"
layout: Meetup
path: "/2018/02/13/fr/human-talks-février-2018/"
---
## Talk #1 Comment la visite d'une usine m'a aidé à construire des meilleurs sites web

Problème: un sprint sur 2 était en echec

Une usine est une chaîne de montage. Lorsqu'un travailleur est en galère, il déclenche le `Andon` -> la chaîne s'arrête totalement. On fixe le problème, puis la chaîne redémarre. 

Réduction entre le moment où le problème arrive et celui où le problème est remonté.

Même principe appliqué au développement. 

Problèmes remontés :
- reproduire le bug est long
- route mal configurée
- `npm install` non fait

Solutions:
- bookmarklet pour dumper la conf d'une page
- live template de code dans IntelliJ
- script de vérification de bonne configuration de projet

## Talk #2 The innovation "coffin corner"
Plus une boîte grandit, moins chaque personne ajoute de valeur. 

Plus une boîte grandit, plus il y a besoin de personnes pour faire quelque chose. 

[https://en.wikipedia.org/wiki/Coffin_corner_(aerodynamics)](https://en.wikipedia.org/wiki/Coffin_corner_\(aerodynamics\))

Dans le coffin corner, il y a trop de à côté (bugs, feature requests, infra à maintenir) pour continuer à innover. 

Solution: for kernel en petits pôles innovant pour continuer à avancer.

## Talk #3 HTTP Caching
- CDN: Shared cache. Une requête passe, le reste est servi par le cache
- Cache navigateur (local)

### Headers
`Expires`, `Pragma` et `Age` sont obsolète 

ETag: Id unique d'une ressource. A repasser lors d'une requête ultérieure avec `If-None-match`

Cache-control:
- cachability: `private`/`public`/`no-cache`/`no-store`
- expiration: `max-age`
- revalidation: `immutable`/`must-revalidate`

Cache busting avec un parameter supplémentaire dans l'URL. Avec `immutable`, la ressource est cachée indéfiniment si elle ne change pas.

## Talk #4 Search recommandation chez Blablacar
Trouver le meilleur trajet pour une recherche

Solution basique : rule-based algorithm.
- 1 dimension : prix ou proximité
- plusieurs dimensions
- scoring 

Problème : complexité limitée. Difficile à porter sur des nouveaux pays.

Solution: machine Learning. Utilise le passé pour prédire le futur.

1. Collecter toutes les données : les recherches, les réponses, les actions des utilisateurs.

2. Nettoyage: enlever les erreurs et les inconsistances. 

3. Magic. Entraîner l'algorithme. Classification.
(Ici: gradient boosted trees)

4. Validation du modèle.

Mise en place de l'algorithme.

Besoins:
- Java et python
- Rapide 
- Performant
- Robuste

https://medium.com/blablacar-tech/fast-machine-learning-predictions-6856b3623e5

XGBoost + XGBoost4j

Dernière étape : A/B test
