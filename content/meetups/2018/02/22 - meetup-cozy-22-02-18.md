---
title: "Meetup Cozy (22/02/2018)"
date: "2018-02-22T23:16:22+01:00"
layout: Meetup
path: "/2018/02/22/fr/meetup-cozy-22-02-18/"
---
## Intro
By Tristan Nitot

Couverture large lors du lancement: le point, nextimpact, techcrunch

## Quoi de neuf et Roadmap 
By Joseph Silvestre

### Quoi de neuf
#### Collect
- connecter plusieurs comptes d'un même connecteur
- meilleure UX lors de l'import des données
- meilleur retour d'un connecteur qui plante
- choisir le dossier de destination 

#### Drive
- meilleur backup des photos
- NEW client de synchronisation linux
- visionneuse de fichiers
- New recherche dans les fichiers. Pas fulltext pour l'instant, uniquement méta donnés. D'autres applications peuvent renforcer les résultats de recherche
- Kit de développement d'apps

### Roadmap
- A2F
- Drive: déplacement, "sélectionner tout", scanner
- facturation: faire payer les gens
- migration facile d'un Cozy à un autre. Pouvoir sortir de Cozy.
- remontées d'erreurs et messages
- Appli contacts 😁
- permettre à une app de modifier les contacts sans y Accéder. Bêta: joseph@cozycloud.cc
- Marché d'applications ! 😀😂😀😂
- Rapprocher les concepts des connecteurs et des apps
- Home Cozy

## Appli Banks
Lancement en bêta le jour de la sortie de la v3. 

Le but: avoir une appli à forte valeur ajoutée disponible. 

Très appréciée en v2. 

Bon reflet de la force de Cozy, avec l'agrégation entre les connecteurs (lien facture SFR depuis la ligne de prélèvement dans l'appli Banks)

Démo

Analyse des remboursements en cours chez la CPAM et les mutuelle.

### Roadmap
- Étoffer les alertes
- Notifications mobiles
- Masquer, annoter, assigner sur un autre mois
- UI de la page d'accueil

### En chantier
- Connecteurs en JavaScript
- Catégorisation automatique

## Catégorisation et IA communautaire 

But: s'affranchir de Linxo

IA avec ADN Cozy:
- respect des données
- décentralisée
- communautaire

IA entraînée sur les serveurs Cozy, centralisé pour l'instant. Prise de décision décentralisée sur chaque instance Cozy.

Besoin d'utilisateurs pour entraîner le modèle. Sécurité maximale avec Tor, fractionnement des données, etc.

### Vision globale de l'IA chez Cozy
Respect de la logique Cozy:
- Open IA: l'algorithme et les paramètres sont transparents
- Décentralisée: respecter l'anonymat des informations
- ANONYMAT DES DONNÉES 
- contribuer à une Intelligence Collective Artificielle

## Créer une app Cozy
By Cedric Patchane

Historique : le template de la v2 n'était pas fonctionnel. Trucs à modifier pour que ça démarre, compliqué à mettre à jour, pas d'uniformisation des webpacks.

`cozy-hello-world`

Plus simple à utiliser. MAIS toujours compliqué à garder à jour, et les configurations étaient variées et éparpillées.

`create-cozy-app`

Une commande pour bootstrapper son app, sans devoir gérer la conf.

3 modes:
- Preact
- Vue
- Vanilla JS!

=> Application sur mesure

Possibilité de customizer son build.

### Roadmap
- les connecteurs!
- app mobile
- Webpack 4
- Passage à `cozy-client`
- des meetups de dév !
- évolutions et optimisations

Utilisé pour Collect, Store, Contacts. 

## Conclusion
