---
title: "TechLunch #26 Designing for developers (16/01/2019)"
date: "2019-01-16T14:10:18+01:00"
layout: Meetup
path: "/2019/01/16/fr/tech-lunch-26-designing-for-developers-16-01-2019/"
---

3 person panel: Algolia, Datadog, Sqreen

## Why this job? 
Designing for developers is not a goal, designing great products is their goal

## Should designers code? (real question : do you code?)
Mostly HTML & CSS. Not a requirement, but allow designers to understand what's going on.

## Differences between designing for developers VS end users
Developers are knowledgeable users. Design must be clear to avoid veeeery bad consequences.
Developers are more critical and less forgiving.
Avoid black box, explain what's happening. 

## Similarities
Meet end users, have a feedback loop. Workflows are the same, they are end users in the end.

## How did you get onboarded? 
Asking a lot of questions to get the technical background.
Start with smaller improvements, in order to interact with the product.

Use diagrams to get a better understanding of the system.

## first impressions of switching to designing for builders 
Make them think about their end users, put things in perspective 

## How long did it take to feel confortable
Everything is changing quickly (mostly tech, and sometimes target users), so we can't never be fully confortable. It's a long term work (imposteur syndrome)

## How do you think developers perceive you
Depends if developers value design, the role of designers, the perception of developers by designers

## What is obvious for you, but not for newcomers
- Attention to details is really important
- Ask questions!
- Developers love to be involved

## Where do you get inspiration from? 
- https://dribbble.com/
- Books (on design, or not) 
- looks at what people are doing with datas (maps, voting machines, real world usages)
- look at other products

## Closing remarks
- Love your designers :-)
- Embrace ambiguity
- Teach UX to developers

## Q&A
- How do you measure good design? 
Feedbacks (user interviews, KPI, support team)

You don't measure good design, you measure a product's success. 

- How/When do you involve developers? 

Make a meeting/workshop to explain the reason for the design. People are more involved when they understand. 

- Where to find good UI/UX websites for developers?

Some principal sites (https://www.smashingmagazine.com/), but no all-you-need bible, you have to search on blogs and social networks.

- How to be friends with developers who "could have make it too" 

Show your value. You allow them to create quicker, have a better product. You know the metrics, you are here to help them