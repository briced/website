---
title: "TechLunch #29: Field Engineering (04/09/2019)"
date: "2019-09-04T16:47:46+02:00"
layout: Meetup
path: "/2019/09/04/fr/tech-lunch-29-field-engineering-04-09-2019/"
---
[Watch it !](https://www.youtube.com/watch?v=pzBjsM7fFXY)

Discussion with 3 panelists

- Stephen Morse - VP Field Engineering, Algolia
- Malik Baudry - Head of EMEA Mobile Solutions Consultants & Google Assistant Partner Engineering, Google
- Alexis Fritel - Sales Engineer Manager Central and Southern Europe, Box

## Definition

What is Field Engineering ? Simply: helping customer techbinal success: pre-sales, post-sales, solution engineers, solution architects.

Customer facing with technical knowledge.

## Team organization
**Alexis**: 4 people in different timezone. Senior team. No micro management. People with different backgrounds, able to work with other roles.
Need to be seen as expert by the client AND the sales team.

**Stephen**: The most important thing is **mastery** of the product & the processes.

Depend on the field, the output will not be the same.

**Don't do hand over, do transitions**

The customer must be followed and known, not moved from à team to another, with each team not knowing what the other did.

**Malik**: Reduce frictions. Analyse early, to know if your product can do what the client wants.

## What metrics to use ?
Sales metrics. 

**Malik**: OKRs. Define objectives from the top, but let the implementation on the team.

**Stephen**: Coverage (opportunities, percent of business covered). Turnover, occupation.

Create hypotheses on why the metrics are important, and them see if it's true or not. Retire metrics when necessary.

**In the absence of metrics, you're guessing**

**Alexis**: Some things can't be measured. And metrics can't tell all the truth. Long time on a failed pre-sale in not failed work, it's just that you were not selected.

Some metrics are only feelings. Knowledge sharing, for example.

## Handling project transition
**Alexis**: Consulting is involved before the signature. The goal is to prepared the client for the future infos he will have to give, and prepare the dev team if evolutions are needed.

**Stephen**: Focus on the 'why' (will the product help the client ?) and the 'how' (implémentation, do & don't)

Pre-sale: more on the why, but can also prepare the how (for Algolia, Since it's an API, there are questions about the 'how' very early) 
Post-sale: more on the how

**Malik**: they are no transitions, post-sale are involved in the pre-sale, customer engineers are also involved.

## What are qualities needed
**Malik**: sort skills: ability to problems

**Stephen**: they are mostly made, not recruted. Intelligence & curiosity. Work ethic. Prior success (having the behavior of success). Coachability. You're looking for potential.

**Alexis**: love peoples. You'll help them to achieve their objectives. Also, you'll do Sales at some time. And you need to fit with your team, and work with other teams.


