---
title: "Secure Web Paris #3 (04/09/2019)"
date: "2019-09-04T21:25:04+02:00"
layout: Meetup
path: "/2019/09/04/fr/secure-web-paris-3/"
---
## La gazette sécurité
- Vulnérabilité chez Yves Rocher: 2.5M utilisateurs leakés à cause d'une base ElasticSearch non sécurisée (https://threatpost.com/data-leak-impacts-millions/147908/)
- Possibilité d'opt-out de l'analyse des enregistrements Siri (https://thehackernews.com/2019/08/apple-siri-recording-privacy.html)
- 850 000 machines infectées par le ver Retadup. Mais le C&C avait une faille, qui a permis de récupérer la main. (https://decoded.avast.io/janvojtesek/putting-an-end-to-retadup-a-malicious-worm-that-infected-hundreds-of-thousands/)

## Web security (Dashlane)
### Le bug
Bug bounty: Hackerone

Bug dans l'extension (Faille: Universal XSS)

Après tests QA, n'impacte que 3% des utilisateurs. Mais quand même.

Corrigée en 4 heures.

### La feature buguée
New user -> demande d'importer un mdp pour un site parmi une liste (FB, Ebay, LinkedIn, etc).

Communication entre la page dashlane (avec la liste des sites) <-> extension <-> app dashlane

L'objet JS injecté dans la page permet d'appeler l'API. L'instruction est filtrée par une regex, celle du bug, qui permet donc d'injecter du code qui sera exécuté.

### Les leçons
- Ne pas injecter **DashlaneAPI** dans toutes les pages
- Mieux, tout garder dans l'extension, pas sur un site web
- Idéalement, la sécurité doit être un critère d'acceptation de la QA. Mais les QA ne sont pas des chercheurs en sécurité.
- Chance d'avoir des gens actifs à minuit pour corriger -> Formaliser les astreintes

## An encryption story (Tanker)
### A story about cats
- Snapcat: React + Node + S3
- Problème: photos de 3 Go
- Solution: Upload direct sur S3 avec des signed URLs
- Re-problème: photos de chats XXX
- Solution: encryption aveec libsodium-js
- Mais: comment je partage la clé avec le destinataire ?
- Solution: chiffrer la clé symétrique avec une clé asymétrique.
- Mais: s'il perd la clé ? Le multi device ? Le partage avec des gens sans compte ?

LIVE CODONS LA SOLUTION !

### Live code
Douche froide, pub pour Filekit, le nouveau produit de Tanker.
