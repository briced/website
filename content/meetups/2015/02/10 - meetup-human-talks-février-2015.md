---
title: "Meetup Human Talks (10/02/2015)"
date: "2015-02-10T12:59:05+01:00"
layout: Meetup
path: "/2015/02/10/fr/meetup-human-talks-février-2015/"
---
# Design émotionnel :
www.brainrules.com

Définir la personnalité de notre site.

Trouver une mascotte. Qui sourit. Et qui est avenante.

Et qui raconte des conneries.

Ex: Mailchimp, Reddit.

Wufoo envoie des lettres manuscrites.

# Latence applicative:
Temps de réaction: 100 à 500ms.

But du site web: réagir < 1 sec.

100ms = 1% vente en moins (Amazon) ou -5% pages vues (Google)

Netflix: jsong + cache (cf infoq, vidéo de jaffer)

Websockets, SPDY, MQTT

Http en 3G: 300ms en RTD

# Stripe
ISO 8583. Requêtes asynchrones. Réponses pas dans l'ordre. Obligé d'utiliser des corrélations ids.

CAFIS. Japan only. Besoin d'envoyer une requête spéciale si la première timeout. Lié à la connexion TCP.
