---
title: "Introducing the Single Element Pattern"
date: "2018-06-26T11:35:22+02:00"
layout: Link
link: "https://medium.freecodecamp.org/introducing-the-single-element-pattern-dfbd2c295c5d"
tags:
  - Javascript
  - React
---
Une explication claire, détaillée, et avec des exemples, de comment créer des composants React robustes.
