---
title: "The Way We Talk About CSS"
date: "2018-10-14T19:17:22+02:00"
layout: Link
link: "https://www.rachelandrew.co.uk/archives/2018/10/04/the-way-we-talk-about-css/"
tags:
  - CSS
---
Avant, la CSS s&#x27;était compliqué. Et on continue à le penser. Mais aujourd&#x27;hui, ce n&#x27;est plus le cas, et Rachel Andrew nous le rappelle.
