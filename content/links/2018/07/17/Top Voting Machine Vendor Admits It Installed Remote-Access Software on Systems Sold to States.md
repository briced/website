---
title: "Top Voting Machine Vendor Admits It Installed Remote-Access Software on Systems Sold to States"
date: "2018-07-17T16:36:21+02:00"
layout: Link
link: "https://motherboard.vice.com/en_us/article/mb4ezy/top-voting-machine-vendor-admits-it-installed-remote-access-software-on-systems-sold-to-states"
tags:
  - 
---
Tout système électronique sera hacké à un moment. Mais là, fournir un accès distant de base, c&#x27;est vraiment aider les pirates.
