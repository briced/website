---
title: "Examples of everything new in ECMAScript 2016, 2017, and 2018"
date: "2018-04-06T14:54:08+02:00"
layout: Link
link: "https://medium.freecodecamp.org/here-are-examples-of-everything-new-in-ecmascript-2016-2017-and-2018-d52fa3b5a70e"
tags:
  - Javascript
---
Des exemples clairs des récentes fonctionnalités JS
