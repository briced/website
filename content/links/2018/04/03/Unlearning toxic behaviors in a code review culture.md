---
title: "Unlearning toxic behaviors in a code review culture"
date: "2018-04-03T10:43:11+02:00"
layout: Link
link: "https://medium.freecodecamp.org/unlearning-toxic-behaviors-in-a-code-review-culture-b7c295452a3c"
tags:
  - 
---
Comment éviter les comportements négatifs, qui démoralisent les autres développeurs. On l'a tous fait un jour, mais le voir écrit permet de prendre du recul.
