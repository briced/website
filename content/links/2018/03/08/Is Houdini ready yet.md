---
title: "Is Houdini ready yet?"
date: "2018-03-08T22:02:08+01:00"
layout: Link
link: "https://ishoudinireadyyet.com/"
tags:
  - CSS
---
Comprendre comment s&#x27;applique la CSS sur une page, c&#x27;est la mission d&#x27;Houdini. Bientôt dans (tous) les navigateurs!
