---
title: "Addy Osmani - Checklist pour un site performant"
date: "2018-11-25T23:23:32+01:00"
layout: Link
link: "https://dev.to/addyosmani/comment/46n7"
tags:
  - Performance
---
Addy Osmani (qui travaille sur Chrome chez Google) livre une checklist concise et précise de ce qu&#x27;il faut faire pour avoir une application web performante
