---
title: "Maintaining code quality when nobody cares"
date: "2018-02-13T12:40:58+01:00"
layout: Link
link: "https://mkdev.me/en/posts/5-recommendations-on-how-to-maintain-the-code-quality-and-keep-your-self-development-when-nobody-cares"
tags:
  - Code quality
---
Toutes les situations qui poussent à écrire du mauvais code, et comment les éviter, ou s'en extraire
