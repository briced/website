---
title: "Stunning hover effects with CSS variables"
date: "2018-02-26T21:47:23+01:00"
layout: Link
link: "https://blog.prototypr.io/stunning-hover-effects-with-css-variables-f855e7b95330"
tags:
  - CSS
  - Javascript
---
Rendre des boutons magnifiques avec 10 lignes de JS et une CSS moderne, c'est ultra-facile en fait!
