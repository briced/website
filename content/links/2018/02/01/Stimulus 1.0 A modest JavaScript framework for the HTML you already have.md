---
title: "Stimulus 1.0: A modest JavaScript framework for the HTML you already have"
date: "2018-02-01T21:25:18+01:00"
layout: Link
link: "https://m.signalvnoise.com/stimulus-1-0-a-modest-javascript-framework-for-the-html-you-already-have-f04307009130"
tags:
  - Javascript
---
Découverte du jour. Pour ajouter des fonctionnalités dans une page rendues côté serveur, je trouve le concept excellent. A tester!