---
title: "Project Things"
date: "2018-02-07T17:06:53+01:00"
layout: Link
link: "https://blog.mozilla.org/blog/2018/02/06/announcing-project-things-open-framework-connecting-devices-web/"
tags:
  - IoT
---
Mozilla essaye d'ouvrir les portes du monde de l'IoT, qui étaient fermées depuis toujours, sans aucune interopérabilité possible.
