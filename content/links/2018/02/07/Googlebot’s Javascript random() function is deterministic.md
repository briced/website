---
title: "Googlebot’s Javascript random() function is deterministic"
date: "2018-02-07T17:11:05+01:00"
layout: Link
link: "http://www.tomanthony.co.uk/blog/googlebot-javascript-random/"
tags:
  - Javascript
---
Une recherche très intéressante (et une utilisation) du fait que le GoogleBot implémente un random() fixé (et modifie le nombre de secondes par minutes)
