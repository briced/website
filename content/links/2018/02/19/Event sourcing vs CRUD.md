---
title: "Event sourcing vs CRUD"
date: "2018-02-19T09:56:16+01:00"
layout: Link
link: "https://community.risingstack.com/event-sourcing-vs-crud/"
tags:
  - Event sourcing
---
On parle de plus en plus de l'event sourcing. Dans quels cas est-il plus pertinent qu'un CRUD?
