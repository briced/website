---
title: "Keeping an eye on the dashboard"
date: "2015-09-16T20:32:03+02:00"
layout: Link
link: "http://quarterly.demos.co.uk/article/issue-4/keeping-an-eye-on-the-dashboard/"
tags:
  - linux
  - ubuntu
---
Un dashboard simplifie l'information, mais il peut aussi cacher une partie de la vérité.