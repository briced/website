---
title: "Prevent Feature Creep and Bad Product Requests"
date: "2015-09-29T20:32:03+02:00"
layout: Link
link: "http://www.productstrategymeanssayingno.com/"
tags:
  - linux
  - ubuntu
---
Un article assez sympa sur les bonnes raisons de dire non aux incessantes demandes de features.