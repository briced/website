---
title: "Why do we fix bugs?"
date: "2015-09-13T20:32:03+02:00"
layout: Link
link: "https://www.youtube.com/watch?v=k2lJiiDzhaE"
tags:
  - linux
  - ubuntu
---
[@miguelitinho](https://twitter.com/miguelitinho) décrypte notre motivation à **corriger** les bugs. Faire plaisir au chef? Faire plaisir aux utilisateurs? Non...