---
title: "Optimal pull request size"
date: "2017-10-28T22:32:03+02:00"
layout: Link
link: "https://dev.to/bosepchuk/optimal-pull-request-size-600"
tags:
  - git
---
On dit toujours qu'une bonne pull request doit être petite, mais petite comment?