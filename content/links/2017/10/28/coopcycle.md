---
title: "Coopcycle"
date: "2017-10-28T20:32:03+02:00"
layout: Link
link: "https://framablog.org/2017/10/28/coopcycle-le-projet-cooperatif-qui-roule-social/"
tags:
  - opensource
  - social
---
Interview de [CoopCycle](https://coopcycle.org/fr/) par Framasoft. Comment contrer Uber quand même les pouvoirs politiques renoncent.