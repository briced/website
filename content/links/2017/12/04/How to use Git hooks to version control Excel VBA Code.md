---
title: "How to use Git hooks to version control Excel VBA Code"
date: "2017-12-04T12:58:36+01:00"
layout: Link
link: "https://www.xltrail.com/blog/auto-export-vba-commit-hook"
tags:
  - git
  - excel
---
Git sert à archiver du code, majoritairement. Et on peut même s'en servir pour archiver du code VBA dans Excel!