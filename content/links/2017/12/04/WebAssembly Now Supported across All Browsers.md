---
title: "WebAssembly Now Supported across All Browsers"
date: "2017-12-04T14:19:15+01:00"
layout: Link
link: "https://www.infoq.com/news/2017/12/webassembly-browser-support"
tags:
  - webassembly
---
WebAssembly est supporté sur tous les navigateurs (moderne)! Le web rentre dans une nouvelle ère de partage de code, où du Rust pourra échanger des données avec du Python, sans soucis!