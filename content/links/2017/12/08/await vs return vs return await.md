---
title: "await vs return vs return await"
date: "2017-12-08T16:42:01+01:00"
layout: Link
link: "https://jakearchibald.com/2017/await-vs-return-vs-return-await/"
tags:
  - Javascript
---
Une rapide mais excellente explication des différences entre await et return. Attention aux potentiels bugs!
