---
title: "30 seconds of code"
date: "2017-12-12T18:14:32+01:00"
layout: Link
link: "https://github.com/Chalarangelo/30-seconds-of-code"
tags:
  - Javascript
---
Une collection de courtes fonctions Javascript pouvant être utilisées sur tous les projets
