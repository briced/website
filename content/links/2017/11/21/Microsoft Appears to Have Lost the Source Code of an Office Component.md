---
title: "Microsoft Appears to Have Lost the Source Code of an Office Component"
date: "2017-11-21T15:29:00+01:00"
layout: Link
link: "https://www.bleepingcomputer.com/news/microsoft/microsoft-appears-to-have-lost-the-source-code-of-an-office-component/"
tags:
  - security
---
Comment patcher un plugin Office quand on ne peut plus le compiler? Patcher le binaire!