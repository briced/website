---
title: "Am I unique?"
date: "2017-11-19T23:05:24+01:00"
layout: Link
link: "https://amiunique.org/"
tags:
  - vie privée
  - privacy
---
L'IP est une donnée d'identification. Mais sans IP, est-il possible de nous identifier?