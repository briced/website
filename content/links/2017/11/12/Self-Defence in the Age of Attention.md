---
title: "Self-Defence in the Age of Attention"
date: "2017-11-12T14:42:23+01:00"
layout: Link
link: "https://medium.com/on-advertising/self-defence-age-of-attention-c4c0e86e1b9d"
tags:
  - attention
  - zen
---
On parle de plus en plus des applications qui veulent capter notre attention à tout prix. Cet article résume les différentes méthodes pour éviter de se faire voler notre temps précieux.