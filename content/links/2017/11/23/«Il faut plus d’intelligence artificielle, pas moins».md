---
title: "«Il faut plus d’intelligence artificielle, pas moins»"
date: "2017-11-23T12:44:25+01:00"
layout: Link
link: "http://www.liberation.fr/debats/2017/11/22/il-faut-plus-d-intelligence-artificielle-pas-moins_1611837"
tags:
  - AI
  - Intelligence Artificielle
---
Très bonne interview de [Yann LeCun](https://fr.wikipedia.org/wiki/Yann_Le_Cun), un des inventeurs du deep learning, au sujet du futur de l'IA et comment corriger les problèmes de biais qui pourraient poser de sérieux problèmes dans le cas d'une IA d'aide à la décision.