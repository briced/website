---
title: "CSS Specificity Calculator"
date: "2017-11-30T11:35:41+01:00"
layout: Link
link: "https://specificity.keegan.st/"
tags:
  - CSS
---
Vous ne comprenez pas pourquoi votre règle CSS A ne replace pas la règle B. Cet outil vous permet de calculer et comparer la priorité de 2 règles.
