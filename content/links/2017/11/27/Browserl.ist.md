---
title: "Browserl.ist"
date: "2017-11-27T14:47:05+01:00"
layout: Link
link: "http://browserl.ist/"
---
Un outil très simple pour tester sa configuration browserlist, et visualiser les navigateurs qui seront inclus par nos règles.