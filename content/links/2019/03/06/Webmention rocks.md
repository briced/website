---
title: "Webmention rocks"
date: "2019-03-06T15:51:18+01:00"
layout: Link
link: "https://webmention.rocks/"
tags:
  - Web
---
Vous vous souvenez des &quot;trackback&quot; ? C&#x27;est maintenant une spécification officielle du W3C, et ce site vous permettra de tester les différents comportements.
