---
title: "Web Component to generate PDF from OpenAPI Spec"
date: "2019-03-26T13:35:18+01:00"
layout: Link
link: "https://mrin9.github.io/RapiPdf/"
tags:
  - Javascript
---
Tout est dans le titre. Très bon outil à utiliser en plus de vos pages web d&#x27;API.
