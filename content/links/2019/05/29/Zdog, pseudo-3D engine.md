---
title: "Zdog, pseudo-3D engine"
date: "2019-05-29T09:59:57+02:00"
layout: Link
link: "https://zzz.dog/"
tags:
  - Javascript
---
Un moteur 3D pour visualiser des objets en drag &amp; drop. La taille du code ? 2100 lignes seulement.
