---
title: "A JavaScript-Free Frontend"
date: "2019-05-07T11:49:03+02:00"
layout: Link
link: "https://dev.to/winduptoy/a-javascript-free-frontend-2d3e"
tags:
  - Javascript
---
On parle souvent de perfs React ou Angular, mais on oublie que la meilleure des perf pourrait être... de ne pas les utiliser.
