---
title: "10 HTML Elements You Didn't Know You Needed"
date: "2019-05-07T13:49:08+02:00"
layout: Link
link: "https://dev.to/emmawedekind/10-html-element-you-didnt-know-you-needed-3jo4"
tags:
  - HTML
---
A force de trop utiliser div et span partout, on oublie qu&#x27;il existe d&#x27;autres éléments HTML, plus évolués, qui peuvent bien nous aider dans notre monde responsive et multimédia.
