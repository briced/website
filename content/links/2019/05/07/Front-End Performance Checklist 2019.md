---
title: "Front-End Performance Checklist 2019"
date: "2019-05-07T11:30:39+02:00"
layout: Link
link: "https://www.smashingmagazine.com/2019/01/front-end-performance-checklist-2019-pdf-pages/ "
tags:
  - Performance
---
Un article très complet et détaillé sur toutes les métriques et méthodes à utiliser pour améliorer la performance de son front
