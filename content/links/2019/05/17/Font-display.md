---
title: "Font-display"
date: "2019-05-17T11:38:38+02:00"
layout: Link
link: "https://font-display.glitch.me/"
tags:
  - CSS
---
Super article sur le chargement et l&#x27;affichage des fonts customs par les navigateurs. Avec un exemple pour comprendre pourquoi les blocs de texte apparaissent d&#x27;un coup sur certains sites.