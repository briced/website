---
title: "How do neural networks see depth?"
date: "2019-06-11T18:00:12+02:00"
layout: Link
link: "https://www.zdnet.com/article/how-do-neural-networks-see-depth/"
tags:
  - IA
---
On perçoit la 3D grâce à nos 2 yeux. Mais comment font les réseaux de neurones pour extraire cette information d&#x27;une seule image ?
