---
title: "AI Algorithms Need FDA-Style Drug Trials"
date: "2019-08-27T16:26:02+02:00"
layout: Link
link: "https://www.wired.com/story/ai-algorithms-need-drug-trials/"
tags:
  - AI
---
Les médicaments sont testés en profondeur avant d&#x27;être mis sur le marché. Les algorithmes ? Pas du tout. Et pourtant ils sont utilisés partout, et peuvent modifier le comportement des gens, en les exposant à des contenus extrêmes &#x27;parce que ça les fera rester sur notre site&#x27;, par exemple. Faut-il réguler les algorithmes comme des médicaments, du coup ?
