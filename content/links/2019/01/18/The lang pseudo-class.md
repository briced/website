---
title: "The :lang pseudo-class"
date: "2019-01-18T13:51:54+01:00"
layout: Link
link: "https://bitsofco.de/use-the-lang-pseudo-class-over-the-lang-attribute-for-language-specific-styles/"
tags:
  - CSS
---
En 2019, je découvre encore des choses qui fonctionnent depuis IE7. Besoin de styler différemment selon la langue ? Utilisez :lang()
