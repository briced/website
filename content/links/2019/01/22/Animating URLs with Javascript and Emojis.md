---
title: "Animating URLs with Javascript and Emojis"
date: "2019-01-22T22:32:14+01:00"
layout: Link
link: "http://matthewrayfield.com/articles/animating-urls-with-javascript-and-emojis/"
tags:
  - Javascript
---
On pense souvent aux URLs comme quelque chose de figé. C&#x27;est faux, on peut jouer avec ! Des emojis, ou même une barre de progression dans l&#x27;URL, c&#x27;est possible, très simplement.
