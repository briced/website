---
title: "Microsoft loses control over Windows Tiles"
date: "2019-04-17T10:39:33+02:00"
layout: Link
link: "https://www.golem.de/news/subdomain-takeover-microsoft-loses-control-over-windows-tiles-1904-140717.html"
tags:
  - Web
---
Désactiver un domaine sans couper le service qui appelle le domaine, c&#x27;est jamais une bonne idée
