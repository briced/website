---
title: "How IBM Watson Overpromised and Underdelivered on AI Health Care"
date: "2019-04-04T15:28:00+02:00"
layout: Link
link: "https://spectrum.ieee.org/biomedical/diagnostics/how-ibm-watson-overpromised-and-underdelivered-on-ai-health-care"
tags:
  - IA
---
8 ans après l&#x27;annonce de Watson, un retour assez critique sur l&#x27;avancée de cette IA dans le monde médical. Efficacité très variable, produits peu simples à utiliser, l&#x27;IA sert de support, mais on est loin du robot docteur.
