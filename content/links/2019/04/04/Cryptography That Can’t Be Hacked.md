---
title: "Cryptography That Can’t Be Hacked"
date: "2019-04-04T15:49:25+02:00"
layout: Link
link: "https://www.quantamagazine.org/how-the-evercrypt-library-creates-hacker-proof-cryptography-20190402/"
tags:
  - crypto
---
Intéressant, mais je le prends avec des pincettes: ils ont créé le langage qui permet de valider que la librairie est sécurisée. Mais qui valide que le langage est viable ?
