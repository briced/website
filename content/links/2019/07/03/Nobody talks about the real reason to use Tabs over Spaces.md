---
title: "Nobody talks about the real reason to use Tabs over Spaces"
date: "2019-07-03T14:05:46+02:00"
layout: Link
link: "https://www.reddit.com/r/javascript/comments/c8drjo/nobody_talks_about_the_real_reason_to_use_tabs/"
tags:
  - Accessibility
---
Je n&#x27;ai jamais pensé à l&#x27;accessibilité dans le débat Tabs VS Spaces, mais ce post met en avant des éléments très pertinents dans l&#x27;accessibilité du TAB que n&#x27;ont pas les espaces.
