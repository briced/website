---
title: "Les liens trop bien #5"
date: "2018-08-27T22:38:37+02:00"
layout: Post
path: "/2018/08/27/fr/les-liens-trop-bien-5/"
tags:
  - les liens trop bien
---
C'est la fin du mois d'aout, on se remet doucement dans le code. 

Les meetups reprennent doucement (Techlunch cette semaine! BDD la semaine prochaine!).

Bref, c'est la rentrée, c'est le moment de tout changer !

## Ménage de rentrée
Même si on commence à voir apparaitre des timides lois sur l'obsolescence programmée, le constat est clair: [on accumule bien plus de choses qu'avant](https://www.theatlantic.com/technology/archive/2018/08/online-shopping-and-accumulation-of-junk/567985/). 
Des choses utiles ? 
Pas forcément, on achète plus pour le plaisir d'acheter que pour l'objet. 
Il est temps de prendre un peu de recul, et de s'intéresser au minimalisme, physique et numérique. 
Vous regardez souvent les gigas de photos sauvegardées dans le cloud par votre téléphone ? 

(Vous savez quel plastique mettre dans la poubelle jaune? [Moi non plus, mais Libé a fait un petit article sur le sujet](http://www.liberation.fr/france/2018/08/21/quels-plastiques-peut-on-vraiment-mettre-dans-la-poubelle-de-tri_1673596))

## La chimie, c'est pas super joli
Ce weekend, je suis tombé par hasard sur le reportage de ["13h15 le dimanche" sur Paul François](https://www.francetvinfo.fr/replay-magazine/france-2/13h15/13h15-du-dimanche-26-aout-2018_2902501.html), un paysan intoxiqué par un herbicide de Monsanto en 2004. 
Depuis, il se bat pour obtenir réparation devant les tribunaux, et milite pour une agriculture moins chimique, plus axée sur l'alchimie du vivant. 
Un très beau reportage, avec un voyage en Argentine, à la rencontre d'autres agriculteurs brisés par ces produits, 
et également des bébés, prématurés à cause des pesticides.

La génération précédente s'est fait empoisonner par ça, continuerons-nous les même erreurs ?

## Gooleur
Google, mais pour les couleurs ? C'est [Picular](https://picular.co/) !
(en vrai, c'est une recherche Google, qui extrait une couleur de chaque image de résultat) 
(regardez le logo, il prend la couleur du premier résultat!)

## Toot se barre en morceaux
Il faudra que je vous parle d'ActivityPub un jour, c'est un protocole de communication qui est en train de créer une petite révolution dans la connexion entre les gens. 
Twitter et Facebook n'exploiteront plus nos données pendant très longtemps. 
En attendant, F-Droid réfléchit à un [système de commentaires basé sur ActivityPub](https://forum.f-droid.org/t/fdroidpub-let-apps-in-f-droid-toot-to-the-fediverse/3553/6).

## Joujou visu
[Pts](https://ptsjs.org/), une nouvelle librairie capable de générer des animations très classes. 
Je n'ai aucune idée de projet où je pourrais l'utiliser, alors en attendant je [joue avec les exemples](https://ptsjs.org/demo/?name=create.gridcells).

Prenez soin de vous !