---
title: "Les liens trop bien #4"
date: "2018-08-20T18:27:46+02:00"
layout: Post
path: "/2018/08/20/fr/les-liens-trop-bien-4/"
tags:
  - les liens trop bien
---
De retour de vacances, où j'ai bien déconnecté, donc on ré-attaque par une liste light. 
Et comme on a bien eu chaud cet été, et que ça ne changera pas si on ne change pas, 
j'ai lu pas mal d'articles en rapport avec l'écologie!

## De l'écologie propre
Les voitures électriques, c'est (parait-il) la solution à tout. 
Je pense plutôt que le partage de transports est une meilleure solution, 
mais se pose toujours la question de l'écologie dans les batteries au lithium, 
pleines de minéraux rares, coûteux à extraire.
[Kemwatt](http://www.larecherche.fr/start/kemwatt-met-au-point-une-batterie-propre-pour-les-energies-renouvelables) 
travaille sur ce problème, et est en train de développer une batterie avec des électrodes non toxiques, recyclables et biodégradables.

## Changer le monde, ou son compte en banque?
Les startups qui changent le monde, vous en connaissez ? 
Ou vous connaissez plutôt des startups branchouilles, qui lèvent des millions, mais ne servent à rien ? 
Dans un monde en quête de sens, Nicolas Menet et Benjamin Zimmer [s'interrogent sur l'avenir des startups et de l'entreprenariat](https://www.forbes.fr/entrepreneurs/ils-denoncent-la-mascarade-des-start-up-et-proposent-un-nouveau-modele/).

## Une IA qui tue. Les préjugés
On parle souvent d'IA militaires, ou utilisées pour remplacer des employés par des machines. 
Une IA entrainée avec des préjugées propagera ces préjugés. 
Mais si on entrainait une IA pour faire l'inverse ? 
C'est comme ça qu'est née [Quicksilver](https://futurism.com/women-scientists-wikipedia-ai/), 
une IA pour détecter les femmes scientifiques sans article wikipédia, 
et qui sait même créer un brouillon d'article toute seule.

## Ma vie privée, c'est privé
Le gros scandale du moment, c'est encore une fois Google qui nous géolocalise en permanence, 
alors qu'on pensait lui avoir dit de ne pas le faire.
Même en étant dans l'informatique depuis longtemps,
j'ai découvert que Google surveillait toutes les applications que je lançais sur mon téléphone, 
"pour personnaliser mon expérience publicitaire". 
[Cet article de Libération](http://www.liberation.fr/futurs/2018/08/13/google-vous-geolocalise-meme-lorsque-vous-lui-dites-de-ne-pas-le-faire-mais-il-y-a-une-solution_1672447) 
explique la situation, et comment mieux se protéger.

## Grand ménage
Combien avez-vous de mails stockés sur votre messagerie ? 1 000 ? 10 000 ? 100 000 ?
Commençons par se désinscrire des newsletters qu'on ne lit jamais avec [CleanFox](https://www.cleanfox.io/), 
et n'hésitons plus à supprimer les mails de 5Mo avec une photo qu'on ne regardera qu'une seule fois.

A la semaine prochaine!