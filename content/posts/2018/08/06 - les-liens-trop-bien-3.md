---
title: "Les liens trop bien #3"
date: "2018-08-06T18:39:01+02:00"
layout: Post
path: "/2018/08/06/fr/les-liens-trop-bien-3/"
tags:
  - les liens trop bien
---

Et voilà, 3ème semaine et j'ai déjà un jour de retard! Rien n'est parfait, ce planning non plus, mais voici les liens sympas que j'ai pu croiser la semaine dernière.

## Des étoiles dans les yeux.
Que donnerai votre quartier s'il était une planète? [Little big city](https://pissang.github.io/little-big-city/) vous permettra d'explorer ce nouveau monde, qui vous est pourtant familier.

Au cours de mes recherches pour animer du SVG, j'ai découvert http://snapsvg.io/. Licence Apache 2, une API claire et simple, ça donne envie de creuser le sujet!

## Manger bouger
Où pouvez-vous aller en une heure de voiture? Et une heure de transport en commun? https://oalley.net/ (qui utilise OpenStreet Map 💓)

## Recrutement de septembre
Comment faire un bon entretien? Combien d'entretiens faire? Qui fait passer les entretiens? Marco Rogers, après plus de 400 entretiens, [fait le point](http://firstround.com/review/my-lessons-from-interviewing-400-engineers-over-three-startups/) sur ce qui fait un bon entretien, et comment péréniser la méthodologie sur le long terme.

## Le roman policier de l'été
[How an Ex-Cop Rigged McDonald’s Monopoly Game and Stole Millions](https://www.thedailybeast.com/how-an-ex-cop-rigged-mcdonalds-monopoly-game-and-stole-millions), 45 minutes de lecture passionnante, digne d'un film.
