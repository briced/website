---
title: "Les liens trop bien #6"
date: "2018-09-03T22:51:10+02:00"
layout: Post
path: "/2018/09/03/fr/les-liens-trop-bien-6/"
tags:
  - les liens trop bien
---
Ca y est, on est en septembre !
Pour me préparer à l'arrivée de notre premier dev, j'ai pas mal lu d'articles sur le management, donc ça sera le principal thème cette semaine. 
Et pour pouvoir passer plus de temps à l'encadrer, on va aussi parler optimisation des journées de travail.

## Qui aime le management ? Pas grand monde
On commence sur une base sympathique: [les employés trouvent leurs managers inutiles](https://business.lesechos.fr/directions-ressources-humaines/management/developper-son-leadership/0301082887994-les-francais-et-le-management-une-relation-compliquee-317443.php).
Depuis quelques temps, on assiste effectivement à un pivot:
le manager doit guider, sans imposer, car sa vision est considérée comme rarement conforme à la réalité fonctionnelle.
Je suis pas encore dans ce cas (les mains dans le code !), mais il va falloir trouver où mettre le curseur entre tyrannie et laisser-faire.

## Travailler plus pour produire moins ?

Les métriques, c'est super bien pour avoir des chiffres "objectifs" sur le travail.

Le nombre d'utilisateurs, le nombre de lignes de code, le nombre d'heures.
Mais souvent, on arrive à un raisonnement "Si avec Z j'obtiens Y, avec Z x 2, j'aurai Y x 2 !".

Le monde réel n'est jamais comme ça. 

[En suivant ses heures de travail](https://medium.com/@loganclements/how-tracking-my-time-changed-the-way-i-work-4067aefed4af),
Logan Clements a mis en évidence que plus d'heures ne voulait pas dire plus de revenus. 
On peut l'appliquer à plein de choses. 
Combien de fois ai-je bloqué sur un bout de code de 17h à 19h30, en m'acharnant, 
pour corriger le problème en 10 minutes le lendemain matin.

Soyez efficace, travaillez mieux ! (La méthode Pomodoro peut vous aider)

## Stop mails
Les emails, c'est la nouvelle plaie moderne.
On cherche à avoir l'accord du N+1, du N+2 et du N+3, histoire d'être à l'abri en cas de problèmes.
On envoie 25 mails pour être sûr que tout le monde a compris.
On est inscrit à 15 newsletters super intéressantes, mais qu'on ne lit jamais.

STOP

En première étape (surtout si vous revenez de vacances !), on applique la règle des 4D :[Do/Delete/Defer/Delegate](https://medium.com/swlh/how-to-use-the-4-ds-of-effective-time-management-61a7ff4bcf20). Plus besoin de passer les 2 premier jours de retour à faire le ménage.

Ensuite, au quotidien, il est temps de réduire notre utilisation des mails, et passer plus de temps à **produire**.
Et pour cela, on peut utiliser [ces 9 astuces](https://medium.com/swlh/9-powerful-email-productivity-practices-to-adopt-right-now-5a30429eff9f) pour fluidifier notre utilisation des mails. 

## Ménage de code

Maintenant qu'on est prêt à produire, il est temps d'alléger notre code !
La CSS, c'est essentiel, mais on oublie souvent d'optimiser nos images, et nos sites deviennent lents. 
Mais grâce à cet [article sur les images responsives](https://medium.freecodecamp.org/time-saving-css-techniques-to-create-responsive-images-ebb1e84f90d5),
c'est désormais un problème réglé, et on peut aller en résoudre un autre.

## Explosion du web

Ce site est hébergé sur un serveur. Si ce serveur s'arrête, mon site n'est plus disponible. 

C'est assez embêtant.

[IPFS](https://hacks.mozilla.org/2018/08/dweb-building-cooperation-and-trust-into-the-web-with-ipfs/) est un nouveau protocole,
qui vient compléter (et à terme remplacer?) le protocole http, qu'on utilise au quotidien.
Grâce à ce protocole, si vous lisez une page de ce site, et que mon serveur tombe plus tard, 
votre navigateur pourra la fournir à la personne qui voudra lire mon site.
Une résilience bienvenue !

## Et on fait sauter les burgers !
Pour finir, on va admirer ces [magnifiques icones de "Burger menu"](https://codepen.io/ainalem/pen/LJYRxz/),
animés intégralement sans Javascript (en dehors de la classe 'active' mise/enlevée grâce au 'onclick'),
et dont les animations sont vraiment très classe.

A la semaine prochaine !