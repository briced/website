---
title: "Les liens trop bien #8"
date: "2018-09-17T13:09:24+02:00"
layout: Post
path: "/2018/09/17/fr/les-liens-trop-bien-8/"
tags:
  - les liens trop bien
---
Cher journal,

Les prochaines semaines vont être chargées. 
Beaucoup de meetups, et des interactions sociales poussées.
Je ne sais pas si tu me lis, mais saches que je vais un peu moins t'écrire.

Mais en attendant...

## L'humain, après tout

Linus Torvalds, créateur de Linux, et globalement connue pour son attitude assez froide et ses remarques cassantes,
[reconnait avoir dépassé les limites](https://lore.kernel.org/lkml/CA+55aFy+Hv9O5citAawS+mVZO+ywCKd9NQ2wxUmGsz9ZJzqgJQ@mail.gmail.com/)
sur la mailing list de Linux. 
Au delà de la surprise, on y lit un homme, très doué dans son domaine, mais loin de la perfection, et qui a le courage de le dire. Emouvant.

## Javascript démineur

Vous connaissez déjà [Javascript equality table](https://github.com/dorey/Javascript-Equality-Table/),
toujours utile pour convaincre les gens que LA SEULE COMPARAISON VALABLE EN JS EST `===` !
En version plus interactive et fun, j'ai découvert [JS equality game](https://slikts.github.io/js-equality-game/),
qui vous permettra de tester vos connaissances

## La vérité sur le mode du travail

[Ce tweet la résume bien](https://twitter.com/rdutel/status/1040918188886827009).
Moins de blabla, moins de superficiel, concentrons-nous sur l'essentiel.

> "Sur la terre tout a une fonction, chaque maladie une herbe pour la guérir, chaque personne une mission." Proverbe Indien
