---
title: "Les liens trop bien #7"
date: "2018-09-10T17:05:37+02:00"
layout: Post
path: "/2018/09/10/fr/les-liens-trop-bien-7/"
tags:
  - les liens trop bien
---

Cette semaine, j'accueille un nouveau développeur au travail, donc on va parler un peu plus de code !

## T'es viré
Aucun lien avec l'introduction, mais j'ai toujours trouvé compliqué d'annoncer une mauvaise nouvelle.
On essaye au maximum d'éviter le conflit au quotidien, c'est donc un moment difficile lorsqu'on sort de cette zone.
[Boz 🇺🇸](http://boz.com/articles/bad-news.html) a écrit un court article sur le sujet, qui résume les mauvaises tactiques qu'on utilise toujours
, et pourquoi il faut arrêter.

## Apprendre à faire des UIs claires
Dès qu'on enlève Bootstrap, on est souvent perdu.
Comment faire une interface claire ?
Comment mettre en avant les informations sans mettre du rouge partout ?
[Refactoring UI 🇺🇸](https://refactoringui.com/) aggrège les différentes informations utiles pour les développeurs qui veulent creuser le sujet.

## Balade sur Github
La navigation sur Github est toujours laborieuse.
On clique sur un dossier, ça charge, puis on clique sur le suivant, ça charge...
[Octotree 🇺🇸](https://github.com/buunguyen/octotree) est une extension navigateur (Chrome / Firefox / Opera) qui facilite cette navigation,
en rajoutant un navigateur de dossier à l'écran. Plus besoin de remonter 'à la main' de 3 dossiers pour aller chercher les tests !

## Flexbox flexbox, what's you gonna do when ...
Vous connaissez déjà [A Complete Guide to Flexbox 🇺🇸](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) 
pour apprendre les différentes propriétés utiles des Flexbox. 
Mais ça manque d'interactivité. 
En renfort, on peut maintenant rajouter [cet éditeur interactif 🇺🇸](http://www.csstutorial.org/flex-both.html) de CSSTutorial.

## La démo de la fin
Les effets 'machine à écrire', on en voit souvent sur les homes de librairies de dev.
Il ne révolutionne rien, mais [T-writer.js 🇺🇸](https://chriscavs.github.io/t-writer-demo/) a une API claire, et aucune dépendance extérieure.
A garder sous le coude !

> "Be yourself; everyone else is already taken." Oscar Wilde