---
title: "Les liens trop bien #2"
date: "2018-07-30T12:12:05+02:00"
layout: Post
path: "/2018/07/30/fr/les-liens-trop-bien-2/"
tags:
  - les liens trop bien
---

Wouhou, 2ème édition des liens trop bien, pour l'instant je suis régulier! Peu de lectures cette semaine, il fait chaud, et il vaut mieux s'aérer le corps et l'esprit!

## Les infos
[11% des américains](https://thehustle.co/meet-the-11-of-americans-who-dont-use-the-internet/) n'utilisent pas Internet. Par manque d'intérêt parfois, mais aussi par manque de finances.

## L'attribut HTML inconnu
[`<wbr>`](https://developer.mozilla.org/fr/docs/Web/HTML/Element/wbr) permet d'indiquer à quel endroit effectuer un potentiel line break

## Astuces de devs

Débugguer des fonctions anonymes, ça peut être parfois chiant, parce qu'il faut mettre des accolades, un return, etc etc, tout ça pour mettre un `console.log`. Mais grâce à [Reddit](https://www.reddit.com/r/javascript/comments/92l1wa/tiny_tip_for_debugging_anonymous_functions/), j'ai découvert le Comma opérator. Super utile dans cette situation!

Le support des flexbox est une des meilleures choses qui soit arrivé dans le monde du frontend. [Philip Walton](https://philipwalton.github.io/solved-by-flexbox/) a crée un site pour expliquer tous les cas de mises en page qu'on détestait faire avant les flexbox, et la meilleure solution à appliquer aujourd'hui pour régler ces problèmes. (Merci à Ubald pour la découverte!)

## Les nouveaux bookmarks
[La semaine dernière](/2018/07/23/fr/les-liens-trop-bien-1/), je vous ai parlé de Term Sheet, pour générer des gifs de prompt. Cette semaine, j'ai découver [Terminaliser](https://github.com/faressoft/terminalizer), qui permet d'enregistrer votre terminal, puis d'en créer un gif. On arrive au même résultat, mais avec un chemin différent, donc je pense que les 2 peuvent se compléter.

J'utilise [date-fns](https://date-fns.org/) sur mes projets JS, pour manipuler des dates. Cette semaine, on m'a parlé de [day.js](https://github.com/iamkun/dayjs). Je ne suis pas sûr qu'il remplacera date-fns sur mes futurs projets, mais si j'arrive sur des projets avec moment.js, ça pourrait constituer une bonne alternative pour faire maigrir mes builds.

Les livrets de Sqreen sont bien, mais plein de liens. Et ouvrir un lien depuis un livret papier, c'est pas facile facile. Heureusement, j'ai découvert que ces livrets existaient en [version web](https://www.sqreen.io/resources). Si vous cherchez des checklists orientées sécurité (AWS, Ops, Node, HTML/CSS), je vous les recommande fortement.

## La bonne action

Comment aider des animaux en détresse, quand on a pas de temps et pas d'argent? Un petit clic sur [Animalwebaction](http://www.animalwebaction.com/) suffit, ça prend très peu de temps, et ça peut changer les choses pour des petites associations indépendantes avec peu de moyens

## Fun fun fun
C'est l'été, il fait chaud, c'est le moment parfait pour se poser et écouter des podcasts. Le [cosy corner #35](https://soundcloud.com/lecosycorner/35-confortable-en-mouvement) m'a rappelé l'existence de [Universal Paperclips](http://www.decisionproblem.com/paperclips/index2.html) que j'ai donc recommencé!

Et sur téléphone, je joue à [Alto's Odyssey](http://www.altosodyssey.com/) quand j'ai 2 minutes, et [Off the road](http://www.dogbytegames.com/off_the_road.html), un monde ouvert à parcourir avec des courses à faire. Mais je fais plus de la balade que les courses, pour le moment.