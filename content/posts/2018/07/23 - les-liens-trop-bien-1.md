---
title: "Les liens trop bien #1"
date: "2018-07-23T21:57:55+02:00"
layout: Post
path: "/2018/07/23/fr/les-liens-trop-bien-1/"
tags:
  - les liens trop bien
---
Hey! Ca va?

Bon, soyons honnête, à la base j'avais prévu plein de choses pour ce site, écrire un article par semaine, poster des liens, faire des poc... mais la vraie vie, ça prend aussi du temps! Surtout que j'ai changé de boulot il y a peu (je suis CTO chez Akoya, pour l'instant je suis CTO d'une équipe de 1 personne, mais on recrute un [front](https://www.welcometothejungle.co/companies/akoya/jobs/frontend-developer_paris) et un [back](https://www.welcometothejungle.co/companies/akoya/jobs/backend-developer_paris).)

Dooonc on va essayer autre chose. Je lis toujours beaucoup de choses dans la semaine, et je suis très fan des [En Vrac](https://standblog.org/blog/post/2018/07/06/En-Vrac-du-vendredi) de Tristan Nitot, ou des [semaine en pixels](https://blog.stephaniewalter.fr/la-semaine-en-pixels-22-juillet-2018/) de Stéphanie Walter. Je vais donc appliquer la même chose ici, et poster un article par semaine avec un récap des liens sympathiques (tech et moins tech) croisés dans la semaine. C'est parti pour la première édition!

## Les infos
Des machines de vote, connectées à Internet et avec un accès à distance installé? [Ils l'ont fait](https://motherboard.vice.com/en_us/article/mb4ezy/top-voting-machine-vendor-admits-it-installed-remote-access-software-on-systems-sold-to-states), et ne comprennent pas pourquoi c'est une mauvaise idée. Et dire qu'en France on réfléchit au vote électronique... (Rappel: Tout ce qui est électronique sera hacké un jour ou l'autre)

L'IA c'est bien, encore faut-il avoir les données et les modèles qui vont bien. Pour la coupe du monde 2018, [c'était pas encore prêt!](https://www.nouvelobs.com/sport/coupe-du-monde-2018/20180716.OBS9740/mondial-les-algorithmes-des-banques-ont-totalement-loupe-leurs-pronostics.html) A dans 4 ans!

La transhumanisme, futur obligé? [Alain Damasio ne le pense pas](http://www.socialter.fr/fr/module/99999672/682/alain_damasio__qrendre_dsirable_autre_chose_que_le_transhumanismeq), et nous donne sa vision du monde, et les alternatives possibles.

## Les geekeries

Envie de générer des gifs de prompt pour vos articles techniques? [Term Sheets](https://gpoitch.github.io/term-sheets/) est parfait pour ça. (Merci [Nicolas](http://www.nicoespeon.com/) pour la découverte!)

Mais en fait, comment font React ou Vue pour mettre à jour la valeur `c = a+b` alors qu'on a juste changé la valeur de `a`? Cet [article clair et accessible](https://medium.com/vue-mastery/the-best-explanation-of-javascript-reactivity-fea6112dd80d) nous explique tout ça, avec du code à l'appui.

## On se pose et on regarde

Le futur de la CSS, c'est presque de la magie! [Houdini CSS, expliqué par Jean-François Garreau](https://www.youtube.com/watch?v=z8Zhn9zwY10)

## Le nouveau bookmark
En travaillant sur la sécurisation de mes serveurs, je suis tombé sur https://cipherli.st/, qui donne des configs SSL sécurisées et prêtes à copier/coller. Ex-cel-lent.

## Et on finit par des pixels
![Sud de la France](https://brice.coquereau.fr/images/south-of-france.png)
[Sud de la France, par Rgznsk](https://www.behance.net/gallery/68049629/Voxels-The-South-of-France)