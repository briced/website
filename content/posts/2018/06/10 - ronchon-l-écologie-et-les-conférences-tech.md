---
title: "Ronchon: L'écologie et les conférences tech"
date: "2018-06-10T17:55:23+02:00"
layout: Post
path: "/2018/06/10/fr/ronchon-l-écologie-et-les-conférences-tech/"
enVersion: "/2018/06/10/en/rant-tech-conferences-and-ecology/"
tags:
  - editorial
---
Au delà du code, on vit dans un monde réel (tant que le contraire n'est pas prouvé).
Et dans ce monde réel, il y a des journées de conférences, techniques ou non, où des gens se rassemblent pour échanger sur un sujet.

Je ne fais que 2 confs tech dans l'année, mais il suffit de regarder les photos que les gens postent sur les réseaux sociaux pour voir que toutes les conférences ont **les mêmes dysfonctionnements**.

## Les goodies
Au delà du tshirt fabriqué en Chine/Bangladesh, on trouvait dans le sac offert à chaque personne à la dernière conf où j'étais: stickers, sous-bock, lingette pour écran (et son emballage plastique), une clé USB de 4go (**de l'électronique qui sera jamais recyclé o//**)

Je pourrais refuser ce sac, mais les goodies ont déjà été fabriqués, le problème reste le même.
On gaspille des ressources pour des goodies d'assez basse qualité, qui tiendront 2 mois avant de prendre la poussière.

## Les stands
Chaque boîte amène son lot de goodies farfelus, pour se démarquer.
Je me rappelle d'épées laser en plastique dans le passé.
À la dernière conférence où j'étais, il y avait des canards, des mains en plastique pour applaudir (insupportable), des hand spinner bluetooth lumineux.

Là, on a une solution, c'est de ne pas prendre ce qui ne sert pas.
Mais les gens ont ce **réflexe humain d'accumulation**, donc il y aura sûrement plein de trucs distribués hier qui prendront la poussière dès demain.

## Le repas
C'est l'heure du repas ! En général, on retrouve des assiettes en carton (mais qui iront dans la poubelle standard) et couverts en plastique (...).
Et pour nourrir les gens, on prévoit plein de nourriture. Trop. **Gâcher de la viande en 2018**, ça fait mal.

Parlons liquide. Des bouteilles d'eau ! Avec des gobelets en plastique !
On avait une ecocup dans le sac à l'entrée, les gobelets étaient-ils nécessaires ?

## Conclusion
Faire des conférences pour échanger sur les problèmes des dévs (ou d'autres sujets) en générant une quantité monumentale de déchets, il y a un bug dans la matrice.
On ne pourra plus dév **le jour où on ne pourra plus vivre**.

Est-ce qu'il faut créer des pseudo-labels ("No goodies conference" / "Full recyclable buffet") ?

Est-ce qu'il faut un engagement fort des orgas ("On ne prend que des sponsors sans goodies plastiques") ?

Je ne sais pas. Mais **on peut faire mieux que ça**.

PS: Tout n'est pas négatif ! On a aussi eu du miel et des graines de fleurs en goodies!
C'est pas la majorité, mais certains ont déjà fait un chemin mental sur le sujet, et c'est très cool.
