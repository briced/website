---
title: "Rant: Tech conferences and ecology"
date: "2018-06-10T18:07:58+02:00"
layout: Post
path: "/2018/06/10/en/rant-tech-conferences-and-ecology/"
language: en
frVersion: "/2018/06/10/fr/ronchon-l-écologie-et-les-conférences-tech/"
tags:
  - editorial
---
Beyond code, there's a real world we live in (until proven otherwise).
And in this real world, there are conferences, about tech or any other subject, where people gather to discuss on a topic.

I'm only going to 2 tech conferences every year, but looking at all the conferences' photos make me think that **they all have the same issues with ecology**.

## Goodies
It often starts with a tshirt (made in China/Bangladesh), every attendee gets its own share of goodies : stickers, coaster, things wrapped in plastic, sometimes USB drive (**yeah, some electronics that will never be recycled o//**)

I could refuse the bag, but the goodies have already been made, the problem would be the same.
We're wasting resources for low-quality goodies, which will break in 2 months, or collect dust.

## Sponsor's booths

Every sponsor brings its share of eccentric goodies, to stand out.
I remember about plastic laser sword in the past.
Last conference I went to, there were plastic ducks and bluetooth hand spinner with lights.

At this point, there's a solution: don't take what you don't need.
But people have a **tendancy to hoard stuff**, so there will be a lot of things that were given yesterday that will collect dust tomorrow.

## Food
It's lunch time! Most of the time, in paper plates (and no recycling bin) and plastic cutlery (...).
And to feed aaaaaall these people, we must provides plenty of food. Too much. **Wasting meat in 2018**, it hurts.

Let's talk about liquids. Water bottles! With plastic cups!
There was an ecocup in the bag, did we need plastic cups?

## Conclusion
Organizing big conferences, where we talk about (technical or not technical) problems, while creating a monumental amount of waste, to me there's a bug in the matrix.
We won't be able to solve problems **the day we won't be able to live**.

Do we need pseudo-labels ("No goodies conference" / "Full recyclable buffet")?
Do we need a strong commitment from the conference's organisers ("We don't accept sponsors providing plastic goodies")?

I don't know. But **we can do better**.

PS: Not everything is bad, we had honey and flower seeds as goodies!
It's clearly not the majority, but some organisations already had a reflection on a subject, and that's pretty cool.
