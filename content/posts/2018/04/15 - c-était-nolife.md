---
title: "C'était Nolife"
date: "2018-04-15T19:30:27+02:00"
layout: Post
path: "/2018/04/15/fr/c-etait-nolife/"
tags:
  - TV
---
On était en 2007. J'étais encore un étudiant, pas encore diplômé.
Je ne sais plus comment j'en ai entendu parler.
Une nouvelle chaine de télé se lançait!
Une chaine dédiée à la culture japonaise et aux jeux vidéos!

<iframe width="560" height="315" src="https://www.youtube.com/embed/5xsqNSYYOEM?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Le 1er juin 2007, à la fin du compte à rebours (en hexadécimal!), ce fut le lancement.
Et là, j'ai découvert ce qu'était une chaine libre,
organisée avec des bouts de ficelles,
mais qui traitait ses sujets avec un profond respect.

Vous êtes habitués aux présentateurs en chemise et cravate?
Passez votre chemin, Nolife était la chaine qui avait des présentateurs dont ce n'était pas le métier.
Et qui portaient des tshirts! Avec Mario dessus!

![Benoit 101%](https://brice.coquereau.fr/images/benoit_101.jpg)

J'ai découvert les Superplay.
Vous avez déjà vu des joueurs faire des performances exceptionnelles sur un jeu?
Là c'est pareil, sauf que le joueur en question regarde sa propre vidéo de jeu,
et décortique les mécanismes d'un jeu dans ses plus profond détails.
Nolife m'a fait découvrir les danmakus, ce sous-genre des shoot them up,
avec une myriade de tirs à l'écran.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rP5xeO3K6OI?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Vous connaissez les webséries?
Ça n'existait à l'époque.
La première série que j'ai connu et qui s'approchait de ce concept, c'était NerdZ, avec Davy et Mr Poulpe.
Puis sont arrivés Noob et Flander's company.
Et enfin le Visiteur du futur, que vous connaissez sûrement.
Le visiteur du futur, c'est la série qui m'a fait découvrir François Descraques,
Raphael Descraques, Justine Lepottier, Eléonore Costes...
beaucoup de gens qui sont connus aujourd'hui dans le monde des webséries.
Nolife fut la première chaine à les diffuser.

Nolife, c'était aussi des reportages réalisés en collaboration avec des sociétés de productions japonaises.
Au delà du "entre tradition et modernité" qu'on entend à toutes les sauces,
Nolife c'était des reportages qui emmenaient des japonais nous faire visiter leurs villes, leurs magasins,
pour nous faire découvrir tout ce qui fait le Japon, et pas uniquement les cerisiers et le saké.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0ZQvSH5-ASM?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Les premières années, j'étais présent à 19h tous les jours, pour découvrir les nouvelles émissions du jour,
apprendre l'envers du décor avec les "Débug mode", voir les critiques du jour dans "101%",
et enchainer avec l'émission de 19h30.

Puis le temps, le travail (n'être plus étudiant, cette révolution dans le planning!), les autres activités,
m'ont éloigné.
Mais, quand j'allumais Nolife le weekend, je restais toujours dessus, car il y avait toujours un reportage intéressant à regarder.
Puis un autre. Puis un autre. Puis encore un autre.

Et voilà que la chaine annonce sa fin, encore une fois. Comme les fois d'avant, j'ai cru à une blague, 
je me suis dit qu'un repreneur s'était manifesté, ou qu'un nouveau partenaire était arrivé.

Mais pas cette fois.

![La fin de Nolife](https://brice.coquereau.fr/images/nolife_0000.jpg)

Cette fois, c'est la vraie. Nolife s'est arrêté le 8 avril 2018, après une très belle soirée finale pleine d'émotions.
Pour ensuite passer en mode zombie, avec des rediffusions jusqu'à l'arrêt du signal.

Enfin, sauf le Superplay inédit sur Kung-Fu Master du 9 avril.

Et les Collectors Quest (7/8) + Interview She, in the Haze + Superplay Gradius Gaiden inédits du 12 avril.

Et les Superplay Castlevania Adventure + Interview par manu de KORE-EDA Hirokazu inédits du 13 avril.

Et la soirée J-Music en live du 13 avril, avec diffusion des clips choisis par les téléspectateurs.

Et le Collector's Quest (8/8) + Superplay Battle Garegga inédits du 16 avril.

Et je n'ai pas parlé de la parodie de la seule pub qui passait encore pendant la période zombie.
VOus en avez marre de Fertiligène? Voici Fertilinet!

Jusqu'au bout, ils ont été libres et indépendants.

C'était Nolife.

<iframe width="560" height="315" src="https://www.youtube.com/embed/1Lp_1fbSJE0?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>