---
title: "L'init parfait d'un projet JS"
date: "2017-12-24T14:22:25+01:00"
layout: Post
path: "/2018/01/05/fr/l-init-parfait-d-un-projet-js/"
tags:
  - Javascript
---
Le Javascript est un langage dont les frameworks et la manière de programmer évoluent vite.
Mais dans ce grand chaos, qu'on apprend à aimer avec le temps,
on a besoin d'avoir des projets qui vivront des années.
Et quand chacun code avec son style, son indentation (2, 3, 4 espaces? Des tabulations?),
ça devient vite un joyeux foutoir!

**On va donc initialiser un projet qui aura pour but d'automatiser un maximum son style,
afin de permettre aux développeurs de se concentrer sur les vrais problèmes de conceptions,
et non sur des détails de mise en forme.**

Cette configuration fonctionne quelque soit le framework que vous utiliserez.
Les outils que l'on va installer sont des outils qui vivent,
donc je pense qu'il est péférable d'avoir une procédure rejouable à tout moment,
plutôt qu'un projet template qu'on dupliquera, et qui aura toujours la v1 de prettier,
alors qu'on est à la v23...

## TLDR
Vous êtes pressés? Voici le résumé en 10 lignes:
- Utiliser la CLI (Angular / Vue / React),
- Faire un eject selon les besoins du projet,
- Ajouter un [.editorconfig](http://editorconfig.org/) pour éviter les IDE à 2 ou 4 caractères de tabulations
(charset UTF-8 PARTOUT),
- Mettre un .npmrc avec `save-prefix='~'` à minima,
- Mettre un [.nvmrc](https://github.com/creationix/nvm#nvmrc) avec la version de Node,
- Mettre [ESLint](https://eslint.org/) (ou [TSLint](https://palantir.github.io/tslint/)),
- Mettre [Prettier](https://github.com/prettier/prettier),
- Dans le package.json, se faire un script de test, un de build, et des scripts de pre/post tag
```json
"preversion": "npm run test",
"postversion": "git push && git push --tags"
```

Et pour la version détaillée, c'est en dessous!

## Init du projet
Selon votre framework, utilisez la CLI associée, et faites un `eject` selon vos besoins.
Votre base de code est là, on va maintenant faire en sorte qu'elle devienne pérenne.

## Configurer l'IDE
### EditorConfig
Grâce au fichier [.editorconfig](http://editorconfig.org/),
compatible avec tous les principaux IDE (WebStorm, Atom, Brackets, VS Code, Vim, Emacs),
on peut spécifier:
- la taille et le style d'indentation: fini les débats "j'utilise 4 espaces et toi une tabulation, j'ai raison et tu as tort!",
- le charset: fini les projets mi-UTF8, mi-latin1!
- le caractère de fin de ligne: "crlf" pour tout le monde, la paix entre les Windowsiens et les Linuxiens!
- ainsi que d'autres propriétés moins importantes telles que l'insertion d'une ligne vide à la fin du fichier.

On peut avoir un style pour tous les fichiers, et surcharger ce style pour certains fichiers (package.json par exemple).
Avec ça, on a déjà une base commune assez solide!

## Configurer Node
### NVM
J'utilise beaucoup [nvm](https://github.com/creationix/nvm) pour changer de versions entre mes projets.
En mettant la version de node dans un fichier `.nvmrc`,
on peut ensuite lancer la commande `nvm use` pour basculer automatiquement sur cette version.
`node -v > .nvmrc` créera ce fameux fichier.

Bonus: Si vous êtes sous linux, ajoutez le bout de code suivant à votre `.bashrc`,
ça lancera la commande `nvm use` dès que vous arriverez dans un dossier contenant un `.nvmrc`

```bash
## Auto load nvm when there's a .nvmrc file
OLD_PWD=""
promptCommand() {
    if [ "$OLD_PWD" != "$PWD" ] ;
        then
        OLD_PWD="$PWD"
        if [ -e .nvmrc ] ;
            then nvm use;
        fi
    fi
}
export PROMPT_COMMAND=promptCommand
```

### NPM
Maintenant que notre version de node est fixée, passons à npm.
npm a la mauvaise manie d'installer les packages avec une version en caret par défaut (`^1.5.23`).
D'après la [documentation npm sur le sujet](https://docs.npmjs.com/misc/semver),
`^1.5.23` permet les mises à jour sur les versions patchs et mineures.
Sachant que les versions mineures peuvent avoir des modifications impactantes sur notre application,
je préfèrerai avoir une version `~1.5.23` lors de l'installation d'un package.

C'est là qu'entre en jeu `.npmrc`. Dans ce fichier, on peut ajouter la ligne
`save-prefix='~'` pour forcer l'utilisation du tilde.
Je n'utilise pas d'autres paramétrages,
mais n'hésitez pas à lire la [page de documentation](https://docs.npmjs.com/files/npmrc) pour découvrir d'autres options qui pourraient vous servir!

## Configurer son projet
L'aspect Node étant configuré, passons maintenant à la configuration du projet,
en particulier le style du code, au delà des tabulations.

### ESLint
Pour être sûr que tout le monde utilise le même style de code, on va installer
[ESLint](https://eslint.org/) (ou [TSLint](https://palantir.github.io/tslint/) si vous faites du TypeScript).

Un simple `npm i -D eslint` (équivalent de `npm install eslint --save-dev`) suffit à installer eslint.
Pour le configurer, la commande `./node_modules/.bin/eslint --init` vous posera quelques questions,
puis génèrera un fichier de configuration valide.
A partir de là, vous pourrez personnaliser la configuration selon vos envies, la documentation du site est très claire.

### Prettier
Mais on aimerait bien ne pas avoir à faire à la main le formatage qu'on vient de configurer dans ESLint.
C'est là qu'intervient [Prettier](https://prettier.io/)!
Cet outil vous permet de formatter automatiquement votre code.

Pour l'installation, `npm i -D -E prettier` (équivalent de `npm install --save-dev --save-exact prettier`) fait le travail à notre place.

Pour le lancer dans notre projet, j'ai choisi d'ajouter un script npm dans package.json
```json
scripts: {
    'prettify': 'prettier --single-quote --trailing-comma es5 --write "{app,__{tests,mocks}__}/**/*.js"'
}
```
Vous trouverez toujours les options sur [cette page](https://prettier.io/docs/en/cli.html).

Il ne vous reste plus qu'à mettre en accord la configuration de prettier et celle d'ESLint,
pour éviter que le formattage de prettier soit refusé par ESLint.
Une configuration possible est d'utiliser [standard](https://standardjs.com/),
son package [ESLint](https://www.npmjs.com/package/eslint-config-standard)
et son package [Prettier](https://www.npmjs.com/package/prettier-standard)
ensemble, par exemple.
Mais vous pouvez aussi configurer vos projets manuellement, c'est ce que je fais sur les miens.

Une fois arrivé ici, on se dit que Prettier est sympa,
mais que ça serait encore mieux s'il se lançait automatiquement.
Et comme la natu... le monde de l'informatique est bien fait, c'est possible!
En suivant [ce guide](https://prettier.io/docs/en/precommit.html),
on fait en sorte que Prettier se lance à chaque `git commit` automatiquement,
ce qui libère notre cher cerveau de cette charge mentale.

(Au lieu de `["prettier --write", "git add"]` dans la configuration de lint-staged,
on fera plutôt appel à notre script prettify crée plus haut,
et on écrira donc `["npm run prettify", "git add"]` à la place)

### NPM Scripts
Lorsqu'on tague un projet avec `npm version`,
il serait bien de vérifier que les tests passent, pour éviter de livrer une version pourrie.
Et on aimerai bien pusher le tag nouvellement crée automatiquement, au lieu qu'il reste uniquement sur notre git local.

Les scripts npm vont nous aider pour ces 2 tâches!

Grâce aux mots clés `preversion` et `postversion`,
on peut créer des scripts qui se lanceront avant et après la montée de version.
```json
scripts: {
    "preversion": "npm run test",
    "postversion": "git push && git push --tags"
}
```
En preversion, on lance juste notre script de test. S'il échoue,
npm s'arrêtera, et ne taguera pas notre projet.
En postversion, on fait un `git push` pour pousser le fichier package.json qui vient d'être modifié,
et un `git push --tags` pour pousser notre nouveau tag.

Il y a [beaucoup d'autres scripts](https://docs.npmjs.com/misc/scripts)
disponibles pour personnaliser notre build, n'hésitez pas à rajouter d'autres automatisations.

## Conclusion
Assez longue quand on ne connait pas les outils,
cette mise en place de projet permet de garantir un projet avec une syntaxe:
- gérée automatiquement, sans besoin d'actions au quotidien par les développeurs,
- commune à Windows et Linux,
- embarquée dans le projet,
- et modifiable si les besoins de votre projet évoluent.

Comme dit dans l'introduction,
les outils présentés évoluent dans le temps,
donc je ne recommande pas de faire un projet template qu'on clonera à chaque fois,
mais bien de rejouer toutes ces étapes à la création d'un nouveau projet.

Cependant, comme il y a beaucoup de `npm install` et modifications de `package.json`,
on pourrait imaginer avoir un script qui ferait tout ça à notre place.

Je vous laisse ça comme exercise à faire à la maison!
