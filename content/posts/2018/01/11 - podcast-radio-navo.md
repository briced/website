---
title: "Podcast: Radio Navo"
date: "2018-01-11T18:36:35+01:00"
layout: Post
path: "/2018/01/11/fr/podcast-radio-navo/"
tags:
  - podcast
---
Je pense que je n'ai pas besoin de présenter Navo, alias Bruno Muschio,
le scénariste et réalisateur de "Bref", "Bloqués" et "Serge le mytho",
ainsi que le créateur de "La bande pas dessinée".

Il a crée [Radio Navo](https://soundcloud.com/radionavo/sets),
qui propose 2 types d'émissions:
- Marché Parlé, un concept qu'il a crée, où il enregistre un monologue
depuis son téléphone, souvent en marchant dans la rue.
La forme est très brute, mais ça permet de se concentrer sur le message.
Il y parle souvent d'aspects de sa vie, comme l'argent ou les échecs,
avec une analyse à postériori et des réflexions très intéressantes sur son parcours.
Ça dure entre 10 et 35 minutes selons les épisodes.

- Les invités de mon invité sont mes invités.
Là, on est sur une émission plus structurée.
Comme son nom l'indique, Navo invite un ami à lui,
qui invite ses propres amis pour discuter d'un sujet décidé par l'ami de Navo.
On y parle vie sur Twitter, humour, création pour le web, BD, et d'autres sujets divers.
Les épisodes sont découpés en plusieurs parties, mais on peut atteindre 3 heures au total,
selon les invités et les digressions pendant les discussions.

Inactive depuis quelques mois/années,
les épisodes passés restent très intéressants à découvrir ou à ré-écouter,
dans le métro ou sur son canapé.

Bonne écoute!