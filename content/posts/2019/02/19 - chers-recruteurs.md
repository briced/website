---
title: "Chers recruteurs"
date: "2019-02-19T13:01:39+01:00"
layout: Post
path: "/2019/02/19/fr/chers-recruteurs/"
tags:
  - Recrutement
  - RH
---
On se parle de plus en plus ces derniers temps, parce vous cherchez des postes techniques,
et que je suis une personne qui code encore après 10 ans de développement.

Mais, chers recruteurs, vous ne m'aidez pas.

Comme tout le monde, je fais des grosses journées au travail.
Je n'ai pas le temps d'appeler tous les recruteurs pour faire un point sur l'offre qu'ils proposent.
Je n'ai pas forcément l'envie de creuser pour qu'on me délivre au compte-goutte des informations,
pour me rendre compte au bout de 20 messages qu'en fait, le job est à Vélizy.

Donc je filtre comme je peux, avec les informations que j'ai.
Et que ça soit "un leader de la Tech à Paris" ou "un éditeur de logiciels français", ça ne me donne aucune information.
**Que fait le produit ?**
Quel est la taille de l'entreprise ? Quelle est la taille de ma future équipe ? Agilité ou cycle en V ? Angular ou React ?
Pourquoi l'entreprise cherche à grandir ?

En bref, donnez-moi envie de venir !

Depuis un mois, on m'a proposé d'intégrer une "Success story française", un "client qui a de beaux projets dans le cadre d'une forte croissance",
et un "client (final) recherchant sa nouvelle "pépite". C'est très mignon, mais ça ne veut rien dire.

Donc, chers recruteurs, voici ma liste au père noël des informations dont je rêve dans une offre d'emploi:
- Les technos. Pas une liste à rallonge, je m'en contrefous de redux-thunk ou redux-saga, mais les technos essentielles doivent être citées.
S'il y a Angular ET React ET Vue, soit c'est une annonce pour un job pas vraiment clair, soit il faut fuir en courant, parce qu'avoir les 3 technos sur un projet c'est signe de mauvaise direction technique. Mais si c'est parce qu'une migration est en cours, dites-le !
- le lieu, précis. En région parisienne, ça peut rajouter 20 minutes de trajet selon le côté de la ville où sont les locaux.
- le salaire & les avantages, précis encore une fois. Fixe + var, avec un éventuel détail des NDF. Je veux savoir précisément combien j'aurai à la fin du mois.
Les fourchettes "entre 20k et 50k brut" ne m'apportent aucune information, tout comme les "selon le profil".
Il y a des grilles de salaire, le poste a été mis dans le budget, on sait tous qu'il y a des chiffres quelques part :)
- les à côté UTILES. Le babyfoot c'est ridicule. Par contre, je veux savoir si j'ai 1 écran ou 2, si mon bureau peut passer en position debout, bref, si je peux bosser dans des bonnes conditions. J'ai besoin de ~~café~~ thé, devrais-je le payer ?
- Si la boite fait autre chose que du code du code du code. il y a des meetups? Une journée banalisée pour échanger entre tout le monde ?
- et surtout, **SURTOUT**, je veux savoir à quoi sert le projet. L'époque "je suis ingénieur informaticien, je veux coder et c'est tout" est révolue. On veut un travail qui a du sens, qui apporte quelque chose à l'humanité.

Faisons en sorte que 2019 soit plus productif que 2018, explicitons les choses !