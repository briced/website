---
title: "Meetup: Webperf 18/10/2017"
date: "2017-10-22T22:32:03+02:00"
layout: Post
path: "/2017/10/22/fr/meetup-webperf-18-10-2017/"
tags:
  - meetup
  - CSS
---

Mercredi dernier avait lieu le meetup [Web Perf chez Mozilla](https://www.meetup.com/fr-FR/Paris-Webperf-Meetup/events/243846244/),
avec 2 présentations qui s'annonçaient excellentes:
- Comment bien mesurer la performance web?
- Comment obtenir des animations CSS une performance maximale avec la technique FLIP.

Je ne vais faire qu'un résumé succint des présentations, vous avez les vidéos et slides en fin d'article.

# Comment bien mesurer la performance web

Dans cette première présentation, on parlait de mesure de performances.
Aujourd'hui, on veut tous que nos sites soient rapides, mais comment définir ce qu'est un site rapide?

On pense tout de suite au temps de chargement.
Historiquement, on se basait sur l'évènement `onload`, mais avec les images et fonts asynchrones, les webservices de CMS appelés,
cet évènement n'est plus pertinent aujourd'hui, car il ne veut pas dire que les requêtes sont finies.

Le poids de notre site est une métrique intéressante à charger, mais cela ne fait pas tout.
Une SPA moderne, une fois chargée, peut attendre encore de nombreuses secondes avant d'afficher quelque chose.

De nouvelles métriques sont apparues avec le temps:
- le temps de premier rendu ('First Paint'). On mesure l'affichage du premier pixel de la page.
Cette information est accessible par des APIs non officielles (`performance.timing.msFirstPaint` sur IE et `performance.getEntriesByType('paint')` sur Chrome).
Le problème de cette métrique, c'est que le premier pixel peut ne pas être pertinent pour l'utilisateur.
Si la première action est d'afficher la barre de menu, l'utilisateur ne peut toujours rien faire...

- le First Contentful Paint. C'est le moment où on écrit du texte dans la page, pour la première fois.
C'est un peu plus pertinent que le First Paint, mais on se sait toujours pas si c'est du texte pertinent, ou un simple bouton 'Accueil'...

- le SpeedIndex. C'est un score calculé lors de l'affichage du site, en comparant ce qui est affiché et le rendu final attendu.
Pour chaque frame, on va faire ce calcul, et tout additionner pour obtenir le score global.
Par exemple, si sur la frame 4, on a 45% du site final qui est affiché, cela fait un score de 55 (100 - 45) pour cette frame.
Plus ce score est élevé, moins notre site est bon.
Mais attention aux pubs qui peuvent créer problèmes lors du calcul du score, car la pub affichée peut ne pas être la pub de l'image de référence.

- le First Meaningful Paint. C'est le moment où on affiche le plus de 'layouts' visibles, après les fonts.
On considère qu'à ce moment, le site affiche le plus d'informations utiles pour l'utilisateur.

- le charge du CPU. Là, les idées diffèrent. Doit-on mesurer la charge CPU totale? Le nombre de 'forced reflow'? Une moyenne du CPU?

- le Time to interactive. Après le First Contentful paint, on attend que le navigateur ne soit pas interrompu pendant 5 secondes par une tâche de plus de 50ms.
Cela veut dire que la plus grosse partie du rendu est faite, et que l'utilisateur peut utiliser le site sans que ça rame.

- Des mesures adaptées à vos besoins. Youtube mesure le temps d'affichage de la vidéo, car c'est ce qui compte chez eux.
Un journal mesurera l'affichage du contenu de son article, etc etc.
Ce sont des métriques bien plus puissantes que les autres, mais c'est du code à créer et adapter selon les projets, donc avec un coût de maintenance plus élevé.

# La technique FLIP

Après un rappel du fonctionnement d'un navigateur lors de l'affichage du page web (Layout, Paint et Composite),
Freddy nous a expliqué que certaines animations CSS (transform et opacity) sont déléguées au GPU, alors que les autres sont effectuées par le GPU.
On peut 'aider' le navigateur avec la propriété [`will-change`](https://developer.mozilla.org/en-US/docs/Web/CSS/will-change)
pour indiquer les propriétés de l'élément qui vont être modifiées dans le temps.

Aujourd'hui, on cherche à atteindre 60 FPS. Donc 16,6ms de traitement par image. Sur mobile, bien sûr. Gros challenge.

La technique FLIP a pour but d'améliorer la fluidité d'une animation.
Elle consiste, dans la fonction [`requestAnimationFrame`](https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame),
à:
- retenir les positions des éléments qu'on veut animer,
- mettre le DOM dans son état final, en appliquant les classes CSS voulues
(visuellement rien ne change, car le navigateur ne fait rien tant que `requestAnimationFrame` ne termine pas),
- retenir les positions finale des éléments, calculées par le navigateur,
- calculer une transformation pour remettre l'élément à son état initial,
- appliquer cette transformation sur le DOM, de sa valeur initiale jusqu'à 0.

Cela parait compliqué, mais si on décompose avec un exemple d'un tableau A B C qu'on veut animer pour arriver à C A B:
- on retient les positions de A B C
- on applique la CSS pour avoir C A B, mais l'utilisateur voit toujours A B C,
- on retient les positions de C, A et B,
- on calcule une transformation pour remettre les éléments en position A B C. Dans le DOM, les éléments sont toujours en position C A B,
- on applique cette transformation. Dans le DOM on a bien C A B, mais visuellement C A B sont transformés pour afficher A B C,
- on lance l'animation. Nos éléments, affichés en A B C, vont se décaler de transform(X) vers transform(0),
ce qui va visuellement les amener dans l'état C A B.

La vidéo ou les slides vont aideront beaucoup, c'est plus simple à expliquer avec des images!

# Conclusion

Un super meetup, avec beaucoup d'informations super poussées et intéressantes, et le tout dans un lieu exceptionnel.
Bon, par contre, j'ai raté le côté 'succint' de mon résumé, tant pis!

# Ressources
- [Vidéo de la présentation "les moyens de mesurer la performance"](https://drive.google.com/file/d/0B0NOM8uFrgygdjBUcWl5enBQYVE/view)
- [Les slides de la présentation "les moyens de mesurer la performance"](https://www.slideshare.net/jpvincent/mesurer-la-performance-onload-pages)
- [Vidéo de la présentation "la performance des animations de layout"](https://drive.google.com/file/d/0B0NOM8uFrgygdFdmTVBnSUEwTDA/view)
- Les slides de la présentation "la performance des animations de layout":
[Partie 1 sur le rendu du navigateur](http://web-anim-perf-keynote.surge.sh/) et
[Partie 2 sur la technique FLIP](http://flip-keynote.surge.sh/)