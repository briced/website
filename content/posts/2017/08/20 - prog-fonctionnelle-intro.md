---
title: "PF: Introduction à la programmation fonctionnelle"
date: "2017-08-20T22:32:03+02:00"
layout: "Post"
lang: "fr"
path: "/2017/08/20/fr/programmation-fonctionnelle-intro/"
tags:
- programmation fonctionnelle
---

J'adore la programmation fonctionnelle, mais la plupart des ressources sont en anglais. Parlons-en un peu en français, pour changer!

# Qu'est-ce que c'est donc?

La programmation fonctionnelle est un paradigme de programmation (une façon de coder) qui diffère des 2 paradigmes les plus connus:
- La programmation procédurale, où les instructions sont exécutées les unes à la suite des autres, avec parfois un saut à une ligne antérieure ou postérieure.
Ex: BASIC ou COBOL
- La programmation orientée objet, où les fonctions sont regroupées dans des objets (métiers ou techniques).
Ex: Java, PHP, C#

La programmation fonctionnelle est, comme son nom l'indique, un paradigme basé sur les fonctions.
Plus besoin d'objets pour encapsuler, les fonctions sont directement appelées.

A première vue, on se dit que ce style de programmation va être très fouilli, sans objets pour regrouper les fonctionnalités similaires.
Mais ce style de programmation apporte son lot de nouveaux concepts, très intéressants, qui permettent de créer du code clair.
On enlève le besoin de regrouper les fonctions dans des objets, les fichiers dans lesquels seront mis les fonctions suffiront à faire ce tri.

# A qui s'adresse cette série d'articles?

Je pense que cette série d'articles sera difficilement accessibles aux personnes ne sachant pas programmer,
je m'adresse plutôt aux développeurs qui font déjà de l'OOP, et qui voudraient apprendre la programmation fonctionnelle.

Même si vous ne faites que de l'OOP au quotidien, les concepts présentés peuvent être utilisés pour rendre votre code plus robuste et maintenable,
sans changer de langage de programmation. Selon le langage que vous utilisez, cela sera plus ou moins possible,
mais certains concepts, tels que les fonctions pures, peuvent être appliqués partout.

# Au sommaire

Dans les prochains articles, nous parlons de ces concepts:
- [les fonctions en tant que paramètres](/2017/08/21/fr/programmation-fonctionnelle-les-fonctions-en-tant-que-parametres/),
- [l'immutabilité](/2017/08/22/fr/programmation-fonctionnelle-l-immutabilite/),
- [les fonctions pures](/2017/09/08/fr/programmation-fonctionnelle-les-fonctions-pures/),
- [l'application partielle](/2017/09/17/fr/programmation-fonctionnelle-l-application-partielle/)
- et sûrement d'autres concepts que j'oublie lors de l'écriture de cet article.

Les exemples seront écrits en javascript car c'est le langage que j'utilise au quotidien,
et c'est un langage assez simple à comprendre, même pour les non-initiés.

Pour réduire la verbosité, j'utilise la syntaxe ES 2017, donc
```javascript
var a = 3;
var b = 5;
var f = function(a,b) {
    return 3;
}
// Est équivalent à
let a = 3;
let b = 5;
const f = (a,b) => {
    return 3;
}
```
(Pour les puristes: je n'utilise pas `const` pour les variables, car j'ai besoin de les faire muter dans certains exemples.
Cela me permet de garder la même syntaxe tout le temps.)