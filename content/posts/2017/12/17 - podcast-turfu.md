---
title: "Podcast: Turfu"
date: "2017-12-17T18:21:57+01:00"
layout: Post
path: "/2017/12/17/fr/podcast-turfu/"
tags:
  - podcast
---
J'habite et je travaille en région parisienne.
Et quand on est dans cette situation, on pratique les transports en commun, avec des podcasts dans les oreilles!

Je ne sais plus comment je suis arrivé dans le monde des podcasts,
mais aujourd'hui j'écoute de nombreux podcasts sur des sujets non techniques (pour les informations techniques, je préfère la lecture).
Et donc je me suis dit que ça serait pas mal d'en parler ici, pour faire découvrir les trucs sympas et qu'on m'en propose d'autres en commentaires.
(au moment où j'écris cet article, il n'y a pas encore de système de commentaires. Rajoutons ça dans la liste des trucs à faire). BREF!

## Turfu, c'est quoi?

C'est un podcast de [Merci Alfred](https://www.mercialfred.com/), que j'ai découvert avec l'application Hook
(et je n'ai absolument plus aucune idée de comment j'ai découvert Hook...).

Avec des interviews assez courtes (25-30 minutes),
ils donnent la parole à des gens qui remettent en cause la norme globalement admise sur différents sujets
(l'agriculture avec Agricool, pêche, le chômage avec Paul Duan).
C'est très instructif, j'adore le côté "tout le monde a toujours fait comme ça, mais nous on a tout remis en question",
ça donne plein d'idées pour remettre en cause le MONDE CAPITAL... se remettre en cause.
C'est un format mi-monologue, mi-voix off, qui laisse beaucoup de place à la personne interviewée pour parler, et c'est vraiment super cool à écouter.

C'est disponible sur [SoundCloud](https://soundcloud.com/turfu-podcast) et [iTunes](https://itunes.apple.com/fr/podcast/turfu/id1277137839?mt=2).
Have fun!