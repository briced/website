---
title: "DotJS 2017: les tests après la bataille"
date: "2017-12-06T14:38:26+01:00"
layout: Post
path: "/2017/12/06/fr/dot-js-2017-les-tests-après-la-bataille/"
tags:
  - dotjs
  - Javascript
  - conferences
---
Après une courte période de décantation, c'est l'heure de faire le point sur DotJS 2017!
Pour la première fois, je ne vais pas faire de retour "présentation par présentation" (retour qu'on peut retrouver sur d'autres sites),
mais je vais plutôt essayer de réfléchir à ce que ces présentations m'ont apportés.

## DotJS, c'est quoi?
[DotJS](https://www.dotjs.io/) existe depuis 2012. C'est aujourd'hui la plus grosse conférence européenne sur le Javascript,
avec plus de mille personnes présentes.
Personnellement, j'y vais tous les ans depuis l'édition 2013, et j'ai pu voir mes attentes évoluer,
au fur et à mesure de ma montée en compétences, et avec l'évolution (rapide!) de JS ces dernières années.
Les premières années, mon carnet de note était rempli de noms de technologies/outils/méthodes inconnues.
Maintenant, j'y vais plutôt pour consolider mes acquis, et voir la direction prise par le langage.

## 2017: Tests, optimisations et remises en cause
L'an dernier, nous étions encore dans la bataille des frameworks.
Il y avait des présentations d'outils basés sur React,
d'autres sur [le fonctionnement de Vue](https://www.dotconferences.com/2016/12/evan-you-reactivity-in-frontend-javascript-frameworks),
et enfin [l'histoire de la création d'Angular](https://www.dotconferences.com/2016/12/igor-minar-keep-your-minds-open).

Un an plus tard, les esprits se sont calmés, les gens ont enfin compris que Vue est supéri... que tout ça n'est que webcomponents,
qui peuvent interagir entre eux ([Horacio Gonzalez en a parlé, entre autres, au meetup WebComponents](https://www.youtube.com/watch?v=2t_GOiKLeWY)),
et qu'au final la technologie derrière n'a pas grand impact (on peut faire du Flux sur les 3: Redux / Vuex / NgRxStore)

Cette année, les sujets étaient plutôt tournés vers la qualité de code, au-delà des frameworks.

## Du code du code du code
[Web Bos](https://wesbos.com/) avec son explication d'async/await, ou le lightning talk de Di Wu sur "Closure VS Prototype",
ainsi que celui de Patrick Roumanoff sur "FP with async/await", sont des considérations à prendre en compte pour créer du code clair.

Mais maintenant que la bataille des frameworks est terminé, il est aussi temps de tester son code!
C'est là que la présentation de [Trent Willis](https://pretty-okay.com/) sur [QUnit-in-Browser](https://pretty-okay.com/2017/12/04/qunit-in-browser)
m'a fait découvrir ce plugin de QUnit pour lancer des tests dans un vrai navigateur headless,
à la différence de PhantomJS qui simule un navigateur très dépassé, et surtout peu représentatif des vrais navigateurs finaux.
De plus, il n'est [plus maintenu par son créateur](https://github.com/ariya/phantomjs/issues/15105).

Cette présentation a également été l'occasion de découvrir [Pupeteer](https://github.com/GoogleChrome/puppeteer),
pour controller un Chrome headless depuis du code JS.
Et comme on peut accéder aux dev tools, on peut aussi récupérer des **métriques sur le code usage (réel)**,
ou des **métriques de performance**, que j'ai détaillé dans un [article précédent](https://brice.coquereau.fr/2017/10/22/fr/meetup-webperf-18-10-2017/).
Tout cela signifie qu'on peut donc avoir ces métriques stockées au court du temps dans un Sonar/Grafana,
et suivre ça bien mieux que "Oh, j'ai l'impression que ça rame un peu en prod là, non?".

Et en parlant de tests, quid de l'**accessibilité**? On en parle beaucoup, mais le sujet avance peu dans les devs.
Cette année, il y avait 2 présentations sur le sujet (comme disait Christophe Porteneuve "**Take the fucking hint**").
[Marcy Sutton](https://marcysutton.com/) était là pour nous présenter [axe-coconut](https://axe-core.org/coconut/),
dernier ajout aux outils d'accessibilité d'aXe.
Après une démo d'utilisation, on se rend compte que mettre des `aria-label` est un bon début, mais que c'est juste un début,
il y a énormément d'autres choses à prendre en compte pour l'accessibilité.
**Tout comme les TU, il faut intégrer la notion d'accessibilité dans le scope intrinsèque d'une feature**.

## De la méthodo, aussi.
### Une application rapide, c'est quoi?
Après avoir repris les bases (légère, responsive),
[Tom Dale](https://tomdale.net/) nous a expliqué que tous les frameworks avaient tout faux en compilant les templates de l'HTML vers le JS,
et nous a expliqué comment fonctionnait [Glimmer](https://glimmerjs.com/), qui compile les templates vers un format binaire, décodé à l'arrivée.
On peut penser que devoir **décompiler** quelque chose sera affreusement lent, mais c'est moins lent que devoir télécharger un JS bien alourdi
(Un simple `div` devenant `React.createElement('div', null, null)`), et cela permet des optimisations lors de la mise à jour également.
La présentation était très intéressante, **je pense que cette méthode de compilation va se retrouver dans d'autres frameworks** d'ici peu.

### Et un compilateur, comment ça fonctionne?
[Sean Larkin](https://github.com/thelarkinn) a profité de son passage sur scène pour nous expliquer le fonctionnement de Webpack, de l'intérieur.
Super instructif, mais ça serait tellement chiant et pas clair à retranscrire à l'écrit, que je vais m'épargner (et vous épargner) ça!
Et ensuite, il nous a présenté les nouveautés de Webpack 4, notamment le support natif de CSS et WASM, ainsi que du tree-shaking encore plus poussé!

### Et si on se passait des frameworks?
Quand on sait qu'il a crée le framwork Django,
le titre de la présentation d'[Adrian Holovaty](http://www.holovaty.com/) parait très ironique.
Mais c'est son expérience de maintenir un framework que tout le monde utilise différemment
(il ajoutait des fonctionnalités qu'il savait ne jamais utiliser dans ses propres projets!)
qui lui a permis de prendre ce recul, et comprendre qu'il **est plus important d'utiliser des bons patterns, plutôt que des frameworks à tout prix**.

## Around the JS
### L'éthique des outils
J'ai dit qu'il y avait eu 2 présentations sur l'accessibilité.
Celle de [Suz Hinton](http://noopkat.com/) n'était pas axée sur le code, mais sur l'histoire du symbole de l'accessibilité (♿ ou U+267F).
Ce symbole, dans sa version originale, a été considéré comme trop dévalorisant, car il montrait globalement un siège avec une tête humaine.
On est loin de quelque chose plein d'humanité.
Sara Hendren l'a donc [retravaillé](http://sarahendren.com/projects/accessible-icon-project/), et grâce à une campagne de désobéissance civique
(en repeignant par dessus les symboles historiques), ce symbole est maintenant officiellement utilisé dans le métro new-yorkais, ainsi que sur iOS.

En partant de cette histoire, Suz s'est demandé comment on pourrait améliorer l'accessibilité sur le web.
Par exemple, les photos présentes sur le web ne sont pas forcément toujours bien taguées avec une description complète.
Et si, au lieu de miner des bitcoins, on utilisait la **puissance informatique collective pour analyser ces images**,
et créer une base de données libre et utilisable par tous?
Un genre de SETI@Home, mais dédié à l'accessibilité! Tout ça est encore sous forme embryonaire, mais l'idée semble géniale!

### Le web est trop sécurisé
C'est en partant de cette idée étrange que [Feross Aboukhadijeh](https://feross.org/) a construit une page qui a pour but d'être le plus insupportable possible!
En déterrant des vieilles APIs inconnues (`Window.moveTo()`), ainsi que des techniques de controle d'iframe et autres atrocités,
on a vu apparaitre devant nous le pire site du monde: [The annoying site](http://theannoyingsite.com/).

### Le monde réel, pour la sécurité...
[Thomas Watson](https://wa.tson.dk/) a acheté une petite antenne (RTL2832U) sur internet,
et s'est amusé à la manipuler depuis Node pour récupérer les informations envoyées en permanence par les avions (protocole ADS-B).
Avec une démo temps réel, on a vu sur une carte les avions qui transmettaient leur informations en temps réel,
comme si on était une tour de contrôle! Mais attends... ça veut dire que ces informations ne sont pas chiffrées?
Et comme cette antenne permet aussi d'émettre, qu'est-ce qui se serait passé si on avait émis des informations erronées?
Bref, encore une fois, une belle démonstration de **l'absence d'importance qu'on donne à la sécurité dans l'informatique**.

## Conclusion
Pour conclure cette journée, [Brendan Eich](https://brendaneich.com/) nous a présenté une rétrospective de Javascript,
depuis sa création (par Brendan Eich) jusqu'à maintenant, voir même jusqu'à demain, car Brendan nous a présenté les prochaines nouveautés du langage:
- BigInt, pour faire des calculs de précisions avec des grands nombres,
- RegExp lookbehind, ce qui aurait pu me servir quelques fois dans le passé,
- les `import()` dynamiques, nativement,
- les méthodes et attributs privés, sans devoir faire des closures,
- `for await of` pour pouvoir faire une boucle sur un tableau de promises

## Ma conclusion
Après toutes ces présentations, j'en tire la conclusion que le frontend Javascript est arrivé à un bon niveau de maturité,
la prochaine étape est de rendre plus robuste les outils de test,
ce qui est toujours ignoré au début, puis qui semble trèèès pratique quand il faut tester une application complexe créée grâce au langage qui est devenu assez mature pour ça!

Après cette maturation, vient aussi l'époque de la réflexion et de l'optimisation. WebAssembly promet des gains de performances,
ainsi qu'une mutualisation, sans précédents. Demain, codera-t'on encore en Javascript? Avec des frameworks?
Et, au delà de l'aspect technique, quelle sera l'éthique de ce code? Est-ce qu'il creusera encore plus le fossé entre les gens valides et non-valides,
ou est-ce qu'il rebouchera ce fossé en se rendant accessible à tous?

On verra bien ce qui se dira à DotJS 2018!