---
title: "Meetup: Identité décentralisée"
date: "2017-07-02T22:32:03+02:00"
layout: "Post"
lang: "fr"
path: "/2017/07/02/fr/meetup-identites-decentralisees/"
tags:
- meetup
- blockchain
---

Jeudi dernier, j'étais au [Meetup Identité décentralisée](https://www.meetup.com/fr-FR/blockchains/events/240891874/) à la maison du Bitcoin. Le sujet était passionnant, mais complexe, donc en voici un petit résumé, loin d'être exhaustif!

Ce meetup était organisé par l'association [Asseth](http://www.asseth.fr/), qui promeut les solutions techniques à base de blockchain Ethereum.

## En pratique, c'est quoi?
Le numérique a modifié notre rapport à l’identité.
Avant, l'état civil était la seule source d'informations sur le sujet, que l'on peut comparer à une seed dans le monde informatique.
Le passeport était donc un token d'authentification, qui pouvait éventuellement porter des sous-tokens, les visas.

Avec le numérique, nous avons désormais une multitude de flux, donc plus de traces numériques.
Cela créé bien plus de données, qui ont beaucoup de valeurs.
Mais ces nouvelles identités numériques sont beaucoup moins contrôlées, personne ne peut savoir ce que fait Facebook ou Twitter des informations qu'ils possèdent sur nous.
Ces informations peuvent être partagées avec d'autres services (régies publicitaires, services gouvernementaux) sans notre consentement.

L'identité numérique a connu plusieurs évolutions:
- Au début, les informations étaient stockées dans chaque service, sans aucun partage/regroupement possible
- Ensuite est venue l'ère de la fédération, avec [OAuth](https://fr.wikipedia.org/wiki/OAuth) principalement.
L'authentification peut être déléguée, mais les informations restent dans les services dont nous sommes clients.
- De nos jours, de plus en plus de services utilisent [OpenID](https://fr.wikipedia.org/wiki/OpenID), qui permet de choisir quelles données sont partagées.
- L'état à atteindre: le self sovereign identity. Les services tiers ne gèrent pas notre identité, mais ils peuvent valider l’information que je veux partager au service A.

Cette évolution est décrite dans Christopher Allen, dans son article (en anglais)
[The Path to Self-Sovereign Identity](http://www.lifewithalacrity.com/2016/04/the-path-to-self-soverereign-identity.html), que je vous recommande fortement.

Des implémentations de cette nouvelles gestions d'identité existent, notamment:
- [Blockstack](https://blockstack.org/)
- [Uport](https://www.uport.me/)
- [Sovrin](https://sovrin.org/)
- [Aevatar](https://aevatar.com/)

## Et techniquement?
### Les termes

Avant de commencer un exemple, il est important de définir certains termes:
- DID (decentralized ID): un identifiant permettant de cibler un DDO.
Utilise la syntaxe URN, décrite dans la [RFC 2141](https://tools.ietf.org/html/rfc2141).
- DDO (DID description object): un ensemble d'informations
- DID record: un DID + le DDO associé

Chaque DID record est chiffré avec la clé privée de l'utilisateur que ces informations concernent.
Idéalement, chaque DID record sera chiffré avec un certificat différent des autres.

### Les implémentations
Une première implémentation à été faite en 2010, bien avant la blockchain, pour un système anonyme de validation de présence au cours.
Cette implémentation a été validée par la CNIL, mais arrêtée en 2014.

Deux solutions différentes avaient été proposées: [U-Prove de Microsoft](https://www.microsoft.com/en-us/research/project/u-prove/), et [Identity mixer d'IBM](http://researcher.watson.ibm.com/researcher/view_group.php?id=664).

Lors de la connexion au service auquel on souhaite accéder, le service demande les informations qui lui sont nécessaires. 
Le but est que le service demande le minimum d'informations qui lui sont nécessaires, sans forcer l'utilisateur à lui révéler toute sa vie.
Par exemple, pour un service de vote en ligne, accessible aux personnes majeures uniquement, le service ne demandera pas la date de naissance, mais il demande une justification que l'utilisateur est majeur.

En réponse, l'utilisateur  enverra sa clé publique, ainsi que les DIDs correspondants aux informations demandées.
Dans les DDOs liées aux DIDs se trouveront les informations permettant de justifier du bon utilisateur.
On remet la clé publique, ainsi que d'éventuels DIDs qui justifient que ce DDO est valide, ce qui créé une chaine de confiance.
On retrouvera également dans le DDO un ou plusieurs endpoints de services.

En conclusion, quelques règles à respecter:
- Eviter la corrélation des clés entre les DDOs
- Eviter la corrélation des pointeurs vers les services extérieurs
- Ne pas stocker de données publiques
- Utiliser des crédentials anonymes, lorsque c'est possible

## Conclusion
Ce meetup très intéressant permettait d'ouvrir le sujet des DIDs et DDOs, en restant accessible, et en donnant des pointeurs pour ceux qui veulent creuser le sujet.

Comme vous avez pu le constater, ma connaissance sur sujet est très sommaire pour l'instant, 
mais vous pourrez trouver plus d'informations sur le [Github de Web of Trust](https://github.com/WebOfTrustInfo/), ainsi que sur [leur site web](http://www.weboftrust.info/)

Au dela de l'aspect technique, cela induit également un changement dans les mentalités, 
car les services ne doivent plus essayer d'obtenir le maximum d'informations possible,
comme on peut le voir aujourd'hui avec de nombreux sites,
mais limiter aux maximums leurs demandes.

Cela passe par une évolution des mentalités, et une sensibilisation des gens à la gestion de leur vie privée, 
mais c'est un argument commercial fort en cette époque où tous les services connaissent des failles un jour ou l'autre.