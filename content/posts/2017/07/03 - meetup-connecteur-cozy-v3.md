---
title: "Meetup: Porter un connecteur Cozy v2 vers v3"
date: "2017-07-03T22:32:03+02:00"
layout: "Post"
lang: "fr"
path: "/2017/07/03/fr/meetup-connecteur-cozy-v3/"
tags:
- meetup
- Cozy
---

Mercredi dernier, j'étais au [Meetup Cozy Cloud](https://www.meetup.com/fr-FR/Meetup-des-utilisateurs-de-Cozy-Cloud-en-France/events/240837430/) dans leurs locaux, pour apprendre à créer un connecteur pour Cozy v3.

Ce meetup était un meetup au format atelier, c'est à dire avec une partie pratique. Allez hop, on ramène son PC, ou pas, et ses idées de connecteurs!

## Un connecteur Cozy, qu'est-ce donc?
[Cozy](https://cozy.io/) a pour objectif de devenir le nouveau coffre-fort open-source de monsieur tout le monde.
Avec un [immense respect pour la vie privée de ses utilisateurs](https://cozy.io/fr/privacy/), Cozy se veut le point central de notre vie numérique.

Pour cela, il faut être capable d'importer de nombreuses données variées (comptes bancaires, factures, photos, etc) dans votre espace Cozy. Et c'est à ça que servent les connecteurs.

Un connecteur Cozy est donc un petit programme, chargé d'importer vos données depuis une source (Sosh, EDF, Améli, Trainline) dans votre espace Cozy.

Pour pouvoir se déployer à grande échelle, Cozy a dû ré-écrire entièrement son code entre la v2 et la v3, ce qui fait que les connecteurs v2 ne sont pas directement utilisables avec la v3.
Mais cela est très simple à porter.

## Porter un connecteur
En utilisant le [code exemple d'un connecteur v3](https://github.com/cozy/cozy-konnector-template),
on trouve un fichier [README_migration](https://github.com/cozy/cozy-konnector-template/blob/master/README_migration.md) qui explique les grandes lignes d'une migration.

Beaucoup de connecteurs v2 ont été fait en CoffeeScript.
Cozy v3 ne supportera pas ce langage directement, il faut donc commencer par transformer les fichiers *.coffee en Javascript, à l'aide de [Decaffeinate](http://decaffeinate-project.org/).
Un coup de `npm i -g decaffeinate && decaffeinate --keep-commonjs konnector.coffee` et ce problème est réglé, on est en Javascript!

L'étape suivante est d'utiliser le package `cozy-konnector-libs` afin d'adapter les imports. Très simple en suivant le tableau de conversion.

A ce moment, on peut tester notre connecteur v3, pour voir si ça fonctionne! `npm run dev`

Si ça fonctionne, félicitations, vous avez porté un connecteur!

En raffinement, on peut ensuite supprimer les fonctions `guardxx` générées par decaffeinate, sachant qu'un `maViariable = a?.b?.c` en CoffeeScript peut se transformer en
```js
let maVariable;
if(a && a.b && a.b.c) {
  maVariable = c;  
}
```
Et on peut supprimer la fonction `guardxx` qui faisait le même travail.

J'ai aussi pensé à tenter la transformation callback -> async/await, mais pour l'instant je n'ai pas trouvé le temps de tester ça, ça fera donc l'objet d'un nouvel article plus tard.

## Conclusion
En 2-3 heures d'atelier, des personnes non techniques ont bien avancées sur leur connecteur, d'autres se sont associés avec des gens de chez Cozy, et ont crée un connecteur fonctionnel.
Pour ma part, j'ai réussi à porter le connecteur Ameli, et sans grande douleur. Comme quoi, une documentation précise et des développeurs sympas, ça fluidifie grandement les contributions!

En attendant, n'hésitez pas à porter des connecteurs, et à venir en parler sur le [forum](https://forum.cozy.io/), les gens y sont très sympathiques!

Happy coding.