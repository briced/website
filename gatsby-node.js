const path = require(`path`)
const _ = require('lodash')
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

	return Promise.all([
		declareBlogPages(graphql, createPage),
		declareMeetupPages(graphql, createPage),
		declareTagPages(graphql, createPage),
	])
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}

const declareBlogPages = (graphql, createPage) => {
	const blogPost = path.resolve(`./src/templates/blog-post.js`)
	return graphql(
		`
      {
        allMarkdownRemark(
          filter: {
            frontmatter: { draft: { ne: true }, layout: { eq: "Post" } }
          }
          sort: { fields: [frontmatter___date], order: DESC }
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                path
                title
              }
            }
          }
        }
      }
    `
	).then(result => {
		if (result.errors) {
			throw result.errors
		}

		// Create blog posts pages.
		const posts = result.data.allMarkdownRemark.edges
		posts.forEach((post, index) => {
			const previous = index === posts.length - 1 ? null : posts[index + 1].node
			const next = index === 0 ? null : posts[index - 1].node

			createPage({
				path: post.node.frontmatter.path,
				component: blogPost,
				context: {
					postPath: post.node.frontmatter.path,
					previous,
					next,
				},
			})
		})
	})
}

const declareMeetupPages = (graphql, createPage) => {
	const meetupTemplate = path.resolve(`./src/templates/meetup.js`)
	return graphql(
		`
      {
        allMarkdownRemark(
          filter: {
            frontmatter: { draft: { ne: true }, layout: { eq: "Meetup" } }
          }
          sort: { fields: [frontmatter___date], order: DESC }
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                path
                title
              }
            }
          }
        }
      }
    `
	).then(result => {
		if (result.errors) {
			throw result.errors
		}

		// Create meetup posts pages.
		const meetupReports = result.data.allMarkdownRemark.edges
		meetupReports.forEach((post, index) => {
			const previous = index === meetupReports.length - 1 ? null : meetupReports[index + 1].node
			const next = index === 0 ? null : meetupReports[index - 1].node

			createPage({
				path: post.node.frontmatter.path,
				component: meetupTemplate,
				context: {
					reportPath: post.node.frontmatter.path,
					previous,
					next,
				},
			})
		})
	})
}

const declareTagPages = (graphql, createPage) => {
	const tagTemplate = path.resolve(`./src/templates/tag.js`)
	return graphql(
		`
      {
        allMarkdownRemark(
          filter: {
            frontmatter: { draft: { ne: true }, layout: { eq: "Post" } }
          }
          sort: { fields: [frontmatter___date], order: DESC }
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                tags
              }
            }
          }
        }
      }
    `
	).then(result => {
		if (result.errors) {
			throw result.errors
		}

		// Create tag pages.
		const tags = {}
		_.each(result.data.allMarkdownRemark.edges, edge => {
			_.each(edge.node.frontmatter.tags, tag => {
				if (!tags[tag]) {
					tags[tag] = []
				}
				tags[tag].push(edge.node.frontmatter.path)
			})
		})
		Object.keys(tags).forEach(tag => {
			createPage({
				path: `/tag/${tag}`,
				component: tagTemplate,
				context: {
					tag: tag,
				},
			})
		})
	})
}
