---
title: "{{name}}"
date: "{{> date }}"
layout: Link
link: "{{url}}"
tags:
  - {{tag}}
---
{{content}}
